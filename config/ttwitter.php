<?php

// You can find the keys here : https://apps.twitter.com/

return [
	'debug'               => false,

	'API_URL'             => 'api.twitter.com',
	'UPLOAD_URL'          => 'upload.twitter.com',
	'API_VERSION'         => '1.1',
	'AUTHENTICATE_URL'    => 'https://api.twitter.com/oauth/authenticate',
	'AUTHORIZE_URL'       => 'https://api.twitter.com/oauth/authorize',
	'ACCESS_TOKEN_URL'    => 'https://api.twitter.com/oauth/access_token',
	'REQUEST_TOKEN_URL'   => 'https://api.twitter.com/oauth/request_token',
	'USE_SSL'             => true,

	'CONSUMER_KEY'        => 'nRfGh8f4CAjCee84Ot6vriBJa',
	'CONSUMER_SECRET'     => '34HxGqePijbvcEqJfj4pv046k8p3IX307fvsxcLnyT8F4hST8V',
	'ACCESS_TOKEN'        => '',
	'ACCESS_TOKEN_SECRET' => '',
];