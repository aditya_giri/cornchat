<!DOCTYPE html>
<html lang="en" class="no-js">
	<head>
		<meta charset="UTF-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge"> 
		<meta name="viewport" content="width=device-width, initial-scale=1"> 
		<title>New Silo | CornChat</title>
		<link rel="stylesheet" type="text/css" href="{{ asset('fullscreen_form/css/normalize.css') }}" />
		<link rel="stylesheet" type="text/css" href="{{ asset('fullscreen_form/css/demo.css') }}" />
		<link rel="stylesheet" type="text/css" href="{{ asset('fullscreen_form/css/component.css') }}" />
		<link rel="stylesheet" type="text/css" href="{{ asset('fullscreen_form/css/cs-select.css') }}" />
		<link rel="stylesheet" type="text/css" href="{{ asset('fullscreen_form/css/cs-skin-boxes.css') }}" />
		<script src="{{ asset('fullscreen_form/js/modernizr.custom.js') }}"></script>
	</head>
	<body>
		<div class="container">

			<div class="fs-form-wrap" id="fs-form-wrap">
				<div class="fs-title">
					<h1>Silo Info</h1>
				</div>
				<form id="myform" class="fs-form fs-form-full" autocomplete="off" method="post">
				<input type="hidden" value="{{ csrf_token() }}" name="_token">
					<ol class="fs-fields">
						<li>
							<label class="fs-field-label fs-anim-upper" for="q1" data-info="without spaces or livonair.com">Enter URL</label>
							<input class="fs-anim-lower" id="q1" pattern="[A-Za-z0-9\S]{1,25}" name="blogname" type="text" placeholder="********.livonair.com" required/>
						</li>
						<li>
							<label class="fs-field-label fs-anim-upper" for="q2">Tagline for your silo</label>
							<input class="fs-anim-lower" id="q2" name="bloginfo" type="text" placeholder="This is amazing silo." required/>
						</li>
					</ol><!-- /fs-fields -->
					<button class="fs-submit" type="submit">Generate My Silo</button>
				</form><!-- /fs-form -->
			</div><!-- /fs-form-wrap -->

		</div><!-- /container -->
		<script src="{{ asset('fullscreen_form/js/classie.js') }}"></script>
		<script src="{{ asset('fullscreen_form/js/selectFx.js') }}"></script>
		<script src="{{ asset('fullscreen_form/js/fullscreenForm.js') }}"></script>
		<script>
			(function() {
				var formWrap = document.getElementById( 'fs-form-wrap' );

				[].slice.call( document.querySelectorAll( 'select.cs-select' ) ).forEach( function(el) {	
					new SelectFx( el, {
						stickyPlaceholder: false,
						onChange: function(val){
							document.querySelector('span.cs-placeholder').style.backgroundColor = val;
						}
					});
				} );

				new FForm( formWrap, {
					onReview : function() {
						classie.add( document.body, 'overview' ); // for demo purposes only
					}
				} );
			})();
			window.$ = document.querySelectorAll.bind(document)
			$("input#UserName").on({
			  keydown: function(e) {
			    if (e.which === 32)
			      return false;
			  },
			  change: function() {
			    this.value = this.value.replace(/\s/g, "");
			  }
			});

		</script>
	</body>
</html>