@if (Auth::guest() || Auth::id() !== $siloinfo['user_id'])
	<?php return \Redirect::to(url('/')) ?>
@endif
<!DOCTYPE html>
<html>
<head>
	<title>Admin | LivOnAir</title>
	<meta charset="utf-8">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
	<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
	<script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
	<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootswatch/3.3.5/flatly/bootstrap.min.css">
	<style type="text/css">
		.container {
			width: 60%;
			margin: auto;
		}
		#editor {
			height: 300px;
		}
	</style>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/ace/1.1.9/ace.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/ace/1.1.9/mode-css.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/ace/1.1.9/snippets/css.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/ace/1.1.9/worker-css.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/ace/1.1.9/theme-terminal.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/ace/1.1.9/ext-beautify.js"></script>
</head>
<body>
	<div class="container">
		@if (Session::has('status'))
			@if(Session::get('status') === "info")
				<div class="alert alert-success">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<strong>Updated</strong> Successfully Updated the changes. If you do not see, then refresh the page.
				</div>
			@elseif(Session::get('status') === "error")
				<div class="alert alert-danger">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<strong>Sorry!</strong> Looks like something went wrong. Please try again.
				</div>
			@endif
		@endif
		<form method="POST" role="form" enctype="multipart/form-data">
			<legend>Edit Your Data</legend>
				<label for="siloname">Silo Name</label>
				<input class="form-control"  id="siloname" name="siloname" type="text" placeholder="{{ $siloinfo['blogname'] }}" disabled="">
			<br/>
				<label>Select Image</label>
				<input name="image" type="file" accept="image/*" >
				@if($siloinfo['image_name'] !== null)
					<img src="https://s3-ap-southeast-1.amazonaws.com/livonair/blog-profiles/{{ $siloinfo['image_name'] }}">
				@endif
			<br/>
				<label for="copyright">Copyright</label>
				<input type="text" class="form-control" name="copyright" id="copyright" value="{{ $siloinfo['copyright'] }}">
			<br />
				<label for="js">Google Analytics Code</label>
				<div class="js" id="editor">{{ $siloinfo['analytics'] }}</div>
				
				<div class="alert alert-info">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<strong>Remember this</strong> Please enter correct analytics code. If you do not have one please get it from Google Analytics. We are currently working on our custom analytics reports. If you do not have one please leave it as it is. If we find any mischivous code, we may reject and even delete your site. Thank you for the support.
				</div>

				<input type="hidden" name="js" id="js">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<button type="submit" class="btn btn-primary" onclick="getvalue()">Submit</button>
		</form>
	</div>	
	<script type="text/javascript">
	    var editor = ace.edit("editor");
	    editor.setTheme("ace/theme/monokai");
	    editor.getSession().setMode("ace/mode/javascript");

	    function getvalue(){
	    	var code = editor.getValue();
	    	$('#js').val(code);
	    }
	</script>
</body>
</html>