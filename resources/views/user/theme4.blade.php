<!DOCTYPE HTML>
<html>
	<head>
		<title>{{ $bloginfo['blogname'] }} | CornChat</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<!--[if lte IE 8]><script src="{{ asset('themes/theme-4/assets/js/ie/html5shiv.js') }}"></script><![endif]-->
		<link rel="stylesheet" href="{{ asset('themes/theme-4/assets/css/main.css') }}" />
		<!--[if lte IE 8]><link rel="stylesheet" href="{{ asset('themes/theme-4/assets/css/ie8.css') }}" /><![endif]-->
				<link rel="stylesheet" type="text/css" href="{{ asset('iconclicks/style.css') }}">
		<link rel="stylesheet" type="text/css" href="{{ asset('iconclicks/font-awesome-4.3.0/css/font-awesome.css') }}">

	</head>
	<body>

		<!-- Content -->
			<div id="content">
				<div class="inner">

					<!-- Post -->
					@if (count($posts) === 0)
						
					
						<article class="box post post-excerpt">
							<header>
								<h2><a href="#">No Posts Found</a></h2>
							</header>
							<p>This silo doesn't have any post. Better see another silo.</p>
			@if (Auth::id() === $bloginfo['user_id'])
				<ul class="actions">
					<li><a href="{{ url('new') }}" class="button ion-ios-compose">New Post</a></li>
				</ul>
			@endif

						</article>
					@else
						@foreach ($posts as $post)
					<!-- Post -->
						<article class="box post post-excerpt" id="{{ $post->slug }}">
							<header>
								<h2><a href="#">{{ $post['title'] }}</a></h2>
							</header>
						<div class="info">
								<ul class="stats">
									<li><a href="#disqus_thread" class="icon fa-comment">...</a></li>
									<li><a href="#" class="icon fa-heart">{{ $post->likeCount }}</a></li>
									<li><a href="http://twitter.com/intent/tweet?status={{ $post->title }}+http://{{ $bloginfo['blogname'] }}.cornchat.ga/#{{ $post->slug }}" class="icon fa-twitter" id="twitter_share">...</a></li>
									<script type="text/javascript">
									jQuery.getJSON('https://cdn.api.twitter.com/1/urls/count.json?url=http://{{ $bloginfo['blogname'] }}/#{{ $post->slug }}', function (data) {
										    jQuery('#twitter_share').text(data.count);
										});
									</script>
								</ul>
							</div>
							{!! $post['post'] !!}
                            @if (!Auth::guest())
                                <button class="cbutton cbutton--effect-milan" onclick="like()">
                                    <i class="cbutton__icon fa fa-fw fa-heart"></i>
                                    <span class="cbutton__text">Favorite</span>
                                </button>
                                <script>
                                    var like = function(){
                                        //Like The Post
                                        <?php if($post->liked()) $post->unlike(); else $post->like(); ?>
                                    }
                                </script>
                            @endif						<div id="disqus_thread"></div>
							<script type="text/javascript">
							    /* * * CONFIGURATION VARIABLES * * */
							    var disqus_shortname = 'cornchat';
							    
							    /* * * DON'T EDIT BELOW THIS LINE * * */
							    (function() {
							        var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
							        dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
							        (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
							    })();
							</script>
							<noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript" rel="nofollow">comments powered by Disqus.</a></noscript>
						</article>
						@endforeach
					@if (Auth::id() === $bloginfo['user_id'])
				<ul class="actions">
					<li><a href="{{ url('new') }}" class="button ion-ios-compose">New Post</a></li>
				</ul>
			@endif

					@endif
				</div>
			</div>

		<!-- Sidebar -->
			<div id="sidebar">

				<!-- Logo -->
					<h1 id="logo"><a href="#">{{ $user['name'] }}</a></h1>

				<!-- Nav -->
					<nav id="nav">
						<ul>
							<li class="current"><a href="#">Latest Post</a></li>
						</ul>
					</nav>

				<!-- Text -->
					<section class="box text-style1">
						<div class="inner">
							<p>{{ $bloginfo['bloginfo'] }}</p>
						</div>
					</section>


				<!-- Copyright -->
					<ul id="copyright">
						<li>&copy; CornChat.</li><li>Design: <a href="http://html5up.net">HTML5 UP</a></li>
					</ul>
			</div>
		<!-- Scripts -->
			<script src="{{ asset('themes/theme-4/assets/js/jquery.min.js') }}"></script>
			<script src="{{ asset('themes/theme-4/assets/js/skel.min.js') }}"></script>
			<script src="{{ asset('themes/theme-4/assets/js/util.js') }}"></script>
			<!--[if lte IE 8]><script src="{{ asset('themes/theme-4/assets/js/ie/respond.min.js') }}"></script><![endif]-->
			<script src="{{ asset('themes/theme-4/assets/js/main.js') }}"></script>
<script type="text/javascript">
    /* * * CONFIGURATION VARIABLES * * */
    var disqus_shortname = 'cornchat';
    
    /* * * DON'T EDIT BELOW THIS LINE * * */
    (function () {
        var s = document.createElement('script'); s.async = true;
        s.type = 'text/javascript';
        s.src = '//' + disqus_shortname + '.disqus.com/count.js';
        (document.getElementsByTagName('HEAD')[0] || document.getElementsByTagName('BODY')[0]).appendChild(s);
    }());
</script>
	</body>
</html>