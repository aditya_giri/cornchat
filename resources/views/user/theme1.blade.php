<!DOCTYPE HTML>
<html>
	<head>
		<title>{{ $bloginfo['blogname'] }} | CornChat</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<!--[if lte IE 8]><script src="{{ ('themes/theme-1/assets/js/ie/html5shiv.js') }}"></script><![endif]-->
		<link rel="stylesheet" href="{{ ('themes/theme-1/assets/css/main.css') }}" />
		<!--[if lte IE 8]><link rel="stylesheet" href="{{ ('themes/theme-1/assets/css/ie8.css') }}" /><![endif]-->
		<!--[if lte IE 9]><link rel="stylesheet" href="{{ ('themes/theme-1/assets/css/ie9.css') }}" /><![endif]-->
				<link rel="stylesheet" type="text/css" href="{{ asset('iconclicks/style.css') }}">
		<link rel="stylesheet" type="text/css" href="{{ asset('iconclicks/font-awesome-4.3.0/css/font-awesome.css') }}">

	</head>
	<body>

		<!-- Header -->
			<section id="header">
				<div class="inner">
					<span class="icon major fa-cloud"></span>
					<h1>{{ $bloginfo['bloginfo'] }}@if (Auth::id() === $bloginfo['user_id'])
					<small><i class="fa fa-pencil"></i></small>
				@endif</h1>
					<p>Blog by - {{ $user['name'] }}</p>
					<ul class="actions">
						<li><a href="#one" class="button scrolly">Discover</a></li>
					</ul>
				</div>
			</section>

		<!-- One -->
		@if (count($posts) === 0)
			{{-- expr --}}
		
			<section id="one" class="main style1">
				<div class="container">
					<div class="row 150%">
						<div class="12u">
							<header class="major">
								<h2>No posts found</h2>
							</header>
							<p>Sorry but this blogger seems to be new. There are no posts here.</p>
			@if (Auth::id() === $bloginfo['user_id'])
				<ul class="actions">
					<li><a href="{{ url('new') }}" class="button ion-ios-compose">New Post</a></li>
				</ul>
			@endif

						</div>
					</div>
				</div>
			</section>
		@else
			@foreach ($posts as $post)
				<section id="one" class="main style1">
					<div class="container">
						<div class="row 150%">
							<div class="12u">
								<header class="major">
									<h2>{{ $post['title'] }}</h2>
								</header>
								<p>{!! $post['post'] !!}</p>
								<ul class="actions">
									<li><a href="{{ url('/post/' . $post['slug']) }}" class="button">Read More</a></li>
																@if (!Auth::guest())
									<button class="cbutton cbutton--effect-milan" onclick="like">
										<i class="cbutton__icon fa fa-fw fa-heart"></i>
										<span class="cbutton__text">Favorite</span>
									</button>
									<span>{{ $post->likeCount }}</span>
                                        <script>
                                            var like = function(){
                                                //Like The Post
                                                <?php if($post->liked()) $post->unlike(); else $post->like(); ?>
                                            }
                                        </script>
@endif
							<div id="disqus_thread"></div>
							<script type="text/javascript">
							    /* * * CONFIGURATION VARIABLES * * */
							    var disqus_shortname = 'cornchat';
							    
							    /* * * DON'T EDIT BELOW THIS LINE * * */
							    (function() {
							        var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
							        dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
							        (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
							    })();
							</script>
							<noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript" rel="nofollow">comments powered by Disqus.</a></noscript>
								</ul>
							</div>
						</div>
					</div>
				</section>
			@endforeach
			@if (Auth::id() === $bloginfo['user_id'])
				<ul class="actions">
					<li><a href="{{ url('new') }}" class="button ion-ios-compose">New Post</a></li>
				</ul>
			@endif

		@endif
		<!-- Two -->
		@if (Auth::id() === $bloginfo['user_id'])
			<ul class="actions">
				<li><a href="{{ url('new') }}" class="button scrolly">New Post</a></li>
			</ul>
		@endif

		<!-- Footer -->
			<section id="footer">
				<ul class="icons">
					<li><a href="#" class="icon alt fa-twitter"><span class="label">Twitter</span></a></li>
					<li><a href="#" class="icon alt fa-facebook"><span class="label">Facebook</span></a></li>
					<li><a href="#" class="icon alt fa-instagram"><span class="label">Instagram</span></a></li>
					<li><a href="#" class="icon alt fa-github"><span class="label">GitHub</span></a></li>
					<li><a href="#" class="icon alt fa-envelope"><span class="label">Email</span></a></li>
				</ul>
				<ul class="copyright">
					<li>&copy; CornChat</li><li>Design: <a href="http://html5up.net">HTML5 UP</a></li>
				</ul>
			</section>

		<!-- Scripts -->
			<script src="{{ ('themes/theme-1/assets/js/jquery.min.js') }}"></script>
			<script src="{{ ('themes/theme-1/assets/js/jquery.scrolly.min.js') }}"></script>
			<script src="{{ ('themes/theme-1/assets/js/skel.min.js') }}"></script>
			<script src="{{ ('themes/theme-1/assets/js/util.js') }}"></script>
			<!--[if lte IE 8]><script src="{{ ('themes/theme-1/assets/js/ie/respond.min.js') }}"></script><![endif]-->
			<script src="{{ ('themes/theme-1/assets/js/main.js') }}"></script>
<script type="text/javascript">
    /* * * CONFIGURATION VARIABLES * * */
    var disqus_shortname = 'cornchat';
    
    /* * * DON'T EDIT BELOW THIS LINE * * */
    (function () {
        var s = document.createElement('script'); s.async = true;
        s.type = 'text/javascript';
        s.src = '//' + disqus_shortname + '.disqus.com/count.js';
        (document.getElementsByTagName('HEAD')[0] || document.getElementsByTagName('BODY')[0]).appendChild(s);
    }());
</script>

	</body>
</html>