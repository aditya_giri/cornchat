<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>New Post | LivOnAir</title>
	<link rel="stylesheet" href="https://storage.googleapis.com/code.getmdl.io/1.0.0/material.indigo-pink.min.css">
	<script src="https://storage.googleapis.com/code.getmdl.io/1.0.0/material.min.js"></script>
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	<script src="{{ asset('editor/ckeditor.js') }}"></script>
	<style type="text/css">
		.container {
			width: 80%;
			margin: 0 auto;
		}
	</style>
</head>
<body>

	<section class="container">
		<form method="post" action="{{ url('admin/new') }}" data-remote>
			<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
			@if(\Session::has('edit'))
				<input name="tags" class="mdl-textfield__input" type="text" id="sample3" value="{{ $post->category }}" />
			@else
				<input name="tags" class="mdl-textfield__input" type="text" id="sample3" />
			@endif
				<label class="mdl-textfield__label" for="sample3">Tags</label>
			</div>
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
			@if(\Session::has('edit'))
				<input name="title" class="mdl-textfield__input" type="text" required value="{{ $post->title }}"/>
			@else
				<input name="title" class="mdl-textfield__input" type="text" required />
			@endif
				<label class="mdl-textfield__label" for="sample3">Title</label>
			</div>
			<textarea name="editor" id="editor" rows="10" cols="80">
				@if(\Session::has('edit'))
					{!! $post->post !!}
				@else
					Hmmmmm. Really LivonAir is so cooooool
				@endif
			</textarea>
			<button type="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect">
			  Create
			</button>
		</form>
	</section>
	<script>
		CKEDITOR.replace( 'editor' );
	</script>
	<script type="text/javascript" src="{{ asset('editor/config.js') }}"></script>
</body>
</html>
