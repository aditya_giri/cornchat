<!DOCTYPE html>
<html>
<head>
	<title>{{ $user['name'] }} | LivOnAir</title>
	<meta charset="utf-8" />
	<meta name="description" content="{{ $bloginfo['bloginfo'] }}">
	<meta property="og:title" content="{{ $bloginfo['blogname'] }} by {{ $user['name'] }} on LivOnAir" />
	<meta property="og:site_name" content="{{ $bloginfo['bloginfo'] }} on LivOnAir" />
	<meta property="og:url" content="http://{{ $bloginfo['blogname'] }}.livonair.com" />
	<meta property="og:description" content="{{$bloginfo['bloginfo']}}" />
	<meta property="og:image" content="https://s3-ap-southeast-1.amazonaws.com/livonair/blog-profiles/{{ $bloginfo['image_name'] }}" />
	<meta name="twitter:card" content="summary">
	<meta name="twitter:url" content="http://{{ $bloginfo['blogname'] }}.livonair.com" >
	<meta name="twitter:title" content="{{ $bloginfo['blogname'] }} by {{ $user['name'] }} on LivOnAir">
	<meta name="twitter:description" content="{{$bloginfo['bloginfo']}}">
	<meta name="twitter:image" content="https://s3-ap-southeast-1.amazonaws.com/livonair/blog-profiles/{{ $bloginfo['image_name'] }}" >
	<link rel="stylesheet" href="https://storage.googleapis.com/code.getmdl.io/1.0.0/material.indigo-pink.min.css">
	<script src="https://storage.googleapis.com/code.getmdl.io/1.0.0/material.min.js"></script>
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	<style type="text/css">
	</style>
	
	<link rel="stylesheet" type="text/css" href="{{ asset('css/blog-new.css') }}">
</head>

<body>

	<!-- Always shows a header, even in smaller screens. -->
	<div class="mdl-layout mdl-js-layout mdl-layout--fixed-header">
	  <header class="mdl-layout__header">
	    <div class="mdl-layout__header-row">
	      <span class="mdl-layout-title">{{ $user['name'] }}</span>
	      <!-- Add spacer, to align navigation to the right -->
	      <div class="mdl-layout-spacer"></div>
	      <!-- Navigation. We hide it in small screens. -->
	      <nav class="mdl-navigation mdl-layout--large-screen-only">
	        
	      </nav>
	    </div>
	  </header>
	  <main class="mdl-layout__content">
	    <div class="page-content">

	<div class="container">
		<div class="user-info">
			@if($bloginfo['image_name'] === "none")
				<img src="{{ asset('no-avatar.jpg') }}" class="img-responsive img-circle img-thumbnail" id="profilepic" width="100">
			@else
				<img src="https://s3-ap-southeast-1.amazonaws.com/livonair/blog-profiles/{{ $bloginfo['image_name'] }}" class="img-responsive img-circle img-thumbnail" id="profilepic" width="100">
			@endif
			<h1>{{ $user['name'] }}</h1>
			<p>{{ $bloginfo['bloginfo'] }}</p>
		</div>
		<div class="user-posts">
			@if (count($posts) === 0)
				<article>
					<h2>No Articles Found</h2>
					<p>We are sorry, but this silo is empty.</p>
				</article>
			@else

				@foreach ($posts as $post)
				@if (strpos($post['post'], "</p>"))
					<?php $str = strpos($post['post'], "</p>") ?>
				@else
					<?php $str = strlen($post['post']) ?>
				@endif
					<article class="epost">
						<h2 class="head"><a href="{{ url('post/' . $post['slug']) }}">{{ $post['title'] }}</a></h2>
						<time>{{ date('F d, Y', strtotime($post['created_at'])) }}</time>
						<div class="post"><p>{!! substr($post['post'], 0, $str) !!}</p></div>
					</article>
				@endforeach
			@endif
		</div>
		@if(\Auth::id() === $bloginfo['user_id'])
		<a class="mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect fab" href="{{ url('admin/new') }}">
		  <i class="material-icons">add</i>
		</a>
		<a class="mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect fab-admin" href="{{ url('admin') }}">
		  <i class="material-icons">account_circle</i>
		</a>
		@endif

		@if(isset($bloginfo['analytics']))
			<script type="text/javascript">
				{!! $bloginfo['analytics'] !!}
			</script>
		@endif
	</div>
	</div>
			<footer class="mdl-mini-footer">
		  <div class="mdl-mini-footer--left-section">
		    <ul class="mdl-mini-footer--link-list">
		      <li>&copy; {{ $bloginfo['copyright'] or $user['name'] }}</li>
		      <li><a href="http://livonair.com">LivOnAir</a></li>
		    </ul>
		  </div>
		</footer>
	  </main>
	</div>
</body>
</html>