<!DOCTYPE HTML>
<html>
	<head>
		<title>{{ $bloginfo['blogname'] }}</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<!--[if lte IE 8]><script src="{{ asset('themes/theme-5/assets/js/ie/html5shiv.js') }}"></script><![endif]-->
		<link rel="stylesheet" href="{{ asset('themes/theme-5/assets/css/main.css') }}" />
		<noscript><link rel="stylesheet" href="{{ asset('themes/theme-5/assets/css/noscript.css') }}" /></noscript>
		<!--[if lte IE 8]><link rel="stylesheet" href="{{ asset('themes/theme-5/assets/css/ie8.css') }}" /><![endif]-->
		<link rel="stylesheet" type="text/css" href="{{ asset('iconclicks/style.css') }}">
		<link rel="stylesheet" type="text/css" href="{{ asset('iconclicks/font-awesome-4.3.0/css/font-awesome.css') }}">

	</head>
	<body>

		<!-- Wrapper-->
			<div id="wrapper">

				<!-- Nav -->
					<nav id="nav">
						<a href="#me" class="icon fa-home active"><span>Home</span></a>
						<a href="#work" class="icon fa-folder"><span>Posts</span></a>
						<a href="#contact" class="icon fa-envelope"><span>Contact</span></a>
					</nav>

				<!-- Main -->
					<div id="main">

						<!-- Me -->
							<article id="me" class="panel">
								<header>
									<h1>{{ $user['name'] }}</h1>
									<p>{{ $bloginfo['bloginfo'] }}</p>
								</header>
								<a href="#work" class="jumplink pic">
									<span class="arrow icon fa-chevron-right"><span>Posts</span></span>
								</a>
							</article>

						<!-- Work -->
							<article id="work" class="panel">
								<header>
									<h2>Posts</h2>
								</header>
								@if (count($posts) === 0)
									<h2>No posts found</h2>
									<p>Sorry but this silo has no grains. Take a look somewhere else.</p>
			@if (Auth::id() === $bloginfo['user_id'])
				<ul class="actions">
					<li><a href="{{ url('new') }}" class="button ion-ios-compose">New Post</a></li>
				</ul>
			@endif

								@else
									@foreach ($posts as $post)
										<h2>{{ $post['title'] }}</h2>
										<p>{!! $post['post'] !!}</p>
                                        @if (!Auth::guest())
                                            <button class="cbutton cbutton--effect-milan" onclick="like">
                                                <i class="cbutton__icon fa fa-fw fa-heart"></i>
                                                <span class="cbutton__text">Favorite</span>
                                            </button>
       										<span>{{ $post->likeCount }}</span>
                                            <script>
                                                var like = function(){
                                                    //Like The Post
                                                    <?php if($post->liked()) $post->unlike(); else $post->like(); ?>
                                                }
                                            </script>
                                        @endif													<div id="disqus_thread"></div>
							<script type="text/javascript">
							    /* * * CONFIGURATION VARIABLES * * */
							    var disqus_shortname = 'cornchat';
							    
							    /* * * DON'T EDIT BELOW THIS LINE * * */
							    (function() {
							        var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
							        dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
							        (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
							    })();
							</script>
							<noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript" rel="nofollow">comments powered by Disqus.</a></noscript>
									@endforeach
												@if (Auth::id() === $bloginfo['user_id'])
				<ul class="actions">
					<li><a href="{{ url('new') }}" class="button ion-ios-compose">New Post</a></li>
				</ul>
			@endif

								@endif
							</article>

						<!-- Contact -->
							<article id="contact" class="panel">
								<header>
									<h2>Contact Me</h2>
								</header>
								<form action="#" method="post">
									<div>
										<div class="row">
											<div class="6u 12u$(mobile)">
												<input type="text" name="name" placeholder="Name" />
											</div>
											<div class="6u$ 12u$(mobile)">
												<input type="text" name="email" placeholder="Email" />
											</div>
											<div class="12u$">
												<input type="text" name="subject" placeholder="Subject" />
											</div>
											<div class="12u$">
												<textarea name="message" placeholder="Message" rows="8"></textarea>
											</div>
											<div class="12u$">
												<input type="submit" value="Send Message" />
											</div>
										</div>
									</div>
								</form>
							</article>

					</div>

				<!-- Footer -->
					<div id="footer">
						<ul class="copyright">
							<li>&copy; CornChat.</li><li>Design: <a href="http://html5up.net">HTML5 UP</a></li>
						</ul>
					</div>

			</div>

		<!-- Scripts -->
			<script src="{{ asset('themes/theme-5/assets/js/jquery.min.js') }}"></script>
			<script src="{{ asset('themes/theme-5/assets/js/skel.min.js') }}"></script>
			<script src="{{ asset('themes/theme-5/assets/js/skel-viewport.min.js') }}"></script>
			<script src="{{ asset('themes/theme-5/assets/js/util.js') }}"></script>
			<!--[if lte IE 8]><script src="{{ asset('themes/theme-5/assets/js/ie/respond.min.js') }}"></script><![endif]-->
			<script src="{{ asset('themes/theme-5/assets/js/main.js') }}"></script>
<script type="text/javascript">
    /* * * CONFIGURATION VARIABLES * * */
    var disqus_shortname = 'cornchat';
    
    /* * * DON'T EDIT BELOW THIS LINE * * */
    (function () {
        var s = document.createElement('script'); s.async = true;
        s.type = 'text/javascript';
        s.src = '//' + disqus_shortname + '.disqus.com/count.js';
        (document.getElementsByTagName('HEAD')[0] || document.getElementsByTagName('BODY')[0]).appendChild(s);
    }());
</script>
	</body>
</html>