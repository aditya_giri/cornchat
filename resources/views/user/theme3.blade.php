<!DOCTYPE HTML>
<html>
	<head>
		<title>{{ $bloginfo['blogname'] }} | CornChat</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<!--[if lte IE 8]><script src="{{ asset('themes/theme-3/assets/js/ie/html5shiv.js') }}"></script><![endif]-->
		<link rel="stylesheet" href="{{ asset('themes/theme-3/assets/css/main.css') }}" />
		<!--[if lte IE 8]><link rel="stylesheet" href="{{ asset('themes/theme-3/assets/css/ie8.css') }}" /><![endif]-->
		<!--[if lte IE 9]><link rel="stylesheet" href="{{ asset('themes/theme-3/assets/css/ie9.css') }}" /><![endif]-->
				<link rel="stylesheet" type="text/css" href="{{ asset('iconclicks/style.css') }}">
		<link rel="stylesheet" type="text/css" href="{{ asset('iconclicks/font-awesome-4.3.0/css/font-awesome.css') }}">

	</head>
	<body>

		<!-- Header -->
			<div id="header">

				<div class="top">

					<!-- Logo -->
						<div id="logo">
							<span class="image avatar48"><img src="images/avatar.jpg" alt="" /></span>
							<h1 id="title">{{ $user['name'] }}</h1>
						</div>

					<!-- Nav -->
						<nav id="nav">
							<ul>
								<li><a href="#top" id="top-link" class="skel-layers-ignoreHref"><span class="icon fa-home">Intro</span></a></li>
								<li><a href="#about" id="about-link" class="skel-layers-ignoreHref"><span class="icon fa-user">Posts</span></a></li>
								<li><a href="#contact" id="contact-link" class="skel-layers-ignoreHref"><span class="icon fa-envelope">Contact</span></a></li>
							</ul>
						</nav>

				</div>

				<div class="bottom">

					<!-- Social Icons -->
						<ul class="icons">
							<li><a href="#" class="icon fa-twitter"><span class="label">Twitter</span></a></li>
							<li><a href="#" class="icon fa-facebook"><span class="label">Facebook</span></a></li>
							<li><a href="#" class="icon fa-github"><span class="label">Github</span></a></li>
							<li><a href="#" class="icon fa-dribbble"><span class="label">Dribbble</span></a></li>
							<li><a href="#" class="icon fa-envelope"><span class="label">Email</span></a></li>
						</ul>

				</div>

			</div>

		<!-- Main -->
			<div id="main">

				<!-- Intro -->
					<section id="top" class="one dark cover">
						<div class="container">

							<header>
								<h2 class="alt">{{ $bloginfo['blogname'] }}</h2>
								<p>{{ $bloginfo['bloginfo'] }}</p>
							</header>

							<footer>
								<a href="#about" class="button scrolly">Move Down</a>
							</footer>

						</div>
					</section>

				<!-- About Me -->
					<section id="about" class="three">
						<div class="container">

							<header>
								<h2>Posts</h2>
							</header>
							@if (count($posts) === 0)
								<h3>No posts found</h3>
			@if (Auth::id() === $bloginfo['user_id'])
				<ul class="actions">
					<li><a href="{{ url('new') }}" class="button ion-ios-compose">New Post</a></li>
				</ul>
			@endif

							@else
								@foreach ($posts as $post)
									<h3>{{ $post['title'] }}</h3>
									<p>{{ $post['post'] }}</p>
                                    @if (!Auth::guest())
                                        <button class="cbutton cbutton--effect-milan" onclick="like">
                                            <i class="cbutton__icon fa fa-fw fa-heart"></i>
                                            <span class="cbutton__text">Favorite</span>
                                        </button>
       									<span>{{ $post->likeCount }}</span>
                                        <script>
                                            var like = function(){
                                                //Like The Post
                                                <?php if($post->liked()) $post->unlike(); else $post->like(); ?>
                                            }
                                        </script>
                                    @endif														<div id="disqus_thread"></div>
							<script type="text/javascript">
							    /* * * CONFIGURATION VARIABLES * * */
							    var disqus_shortname = 'cornchat';
							    
							    /* * * DON'T EDIT BELOW THIS LINE * * */
							    (function() {
							        var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
							        dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
							        (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
							    })();
							</script>
							<noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript" rel="nofollow">comments powered by Disqus.</a></noscript>
									<hr/>

								@endforeach
			@if (Auth::id() === $bloginfo['user_id'])
				<ul class="actions">
					<li><a href="{{ url('new') }}" class="button ion-ios-compose">New Post</a></li>
				</ul>
			@endif
								
							@endif
							

						</div>
					</section>

				<!-- Contact -->
					<section id="contact" class="four">
						<div class="container">

							<header>
								<h2>Contact</h2>
							</header>

							<form method="post" action="#">
								<div class="row">
									<div class="6u 12u$(mobile)"><input type="text" name="name" placeholder="Name" /></div>
									<div class="6u$ 12u$(mobile)"><input type="text" name="email" placeholder="Email" /></div>
									<div class="12u$">
										<textarea name="message" placeholder="Message"></textarea>
									</div>
									<div class="12u$">
										<input type="submit" value="Send Message" />
									</div>
								</div>
							</form>

						</div>
					</section>

			</div>

		<!-- Footer -->
			<div id="footer">

				<!-- Copyright -->
					<ul class="copyright">
						<li>&copy; CornChat. All rights reserved.</li><li>Design: <a href="http://html5up.net">HTML5 UP</a></li>
					</ul>

			</div>

		<!-- Scripts -->
			<script src="{{ asset('themes/theme-3/assets/js/jquery.min.js') }}"></script>
			<script src="{{ asset('themes/theme-3/assets/js/jquery.scrolly.min.js') }}"></script>
			<script src="{{ asset('themes/theme-3/assets/js/jquery.scrollzer.min.js') }}"></script>
			<script src="{{ asset('themes/theme-3/assets/js/skel.min.js') }}"></script>
			<script src="{{ asset('themes/theme-3/assets/js/util.js') }}"></script>
			<!--[if lte IE 8]><script src="{{ asset('themes/theme-3/assets/js/ie/respond.min.js') }}"></script><![endif]-->
			<script src="{{ asset('themes/theme-3/assets/js/main.js') }}"></script>
<script type="text/javascript">
    /* * * CONFIGURATION VARIABLES * * */
    var disqus_shortname = 'cornchat';
    
    /* * * DON'T EDIT BELOW THIS LINE * * */
    (function () {
        var s = document.createElement('script'); s.async = true;
        s.type = 'text/javascript';
        s.src = '//' + disqus_shortname + '.disqus.com/count.js';
        (document.getElementsByTagName('HEAD')[0] || document.getElementsByTagName('BODY')[0]).appendChild(s);
    }());
</script>
	</body>
</html>