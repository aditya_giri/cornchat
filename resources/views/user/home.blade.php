@extends('public')

@section('title')
	Blogs
@stop

@section('css')
<link rel="stylesheet" href="https://storage.googleapis.com/code.getmdl.io/1.0.0/material.indigo-pink.min.css">
<script src="https://storage.googleapis.com/code.getmdl.io/1.0.0/material.min.js"></script>
<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
<style type="text/css">
	.blog-card {
		margin-left: 30%;
	}
</style>
@stop

@section('content')
	@if (count($blogs) !== 0)
		@foreach ($blogs as $blog)
			<div class="mdl-card mdl-shadow--2dp blog-card">
			  <div class="mdl-card__title">
			    <h2 class="mdl-card__title-text">{{ \Auth::getUser()->name }}</h2>
			  </div>
			  <div class="mdl-card__supporting-text">
			    {{ $blog->bloginfo }}
			  </div>
			  <div class="mdl-card__actions mdl-card--border">
			    <a href="http://{{ $blog->blogname }}.livonair.com/admin/" class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect">
			      Admin Page
			    </a>
			  </div>
			</div>
		@endforeach
	@else
		<a class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect blog-card" href="{{ url('/newsilo') }}">
			Create New Silo
		</a>
	@endif
@stop