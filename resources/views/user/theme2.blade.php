<!DOCTYPE HTML>
<html>
	<head>
		<title>{{ $bloginfo['blogname'] }} | CornChat</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<!--[if lte IE 8]><script src="{{ ('themes/theme-2/assets/js/ie/html5shiv.js') }}"></script><![endif]-->
		<link rel="stylesheet" href="{{ ('themes/theme-2/assets/css/main.css') }}" />
		<!--[if lte IE 8]><link rel="stylesheet" href="{{ ('themes/theme-2/assets/css/ie8.css') }}" /><![endif]-->
		<link rel="stylesheet" type="text/css" href="{{ asset('iconclicks/style.css') }}">
		<link rel="stylesheet" type="text/css" href="{{ asset('iconclicks/font-awesome-4.3.0/css/font-awesome.css') }}">

	</head>
	<body>

		<!-- Header -->
			<section id="header">
				<header>
					<span class="image avatar"><img src="{{ asset('themes/theme-2/images/avatar.jpg') }}" alt="" /></span>
					<h1 id="logo"><a href="#">{{ $user['name'] }}</a></h1>
				</header>
				<nav id="nav">
					<ul>
						<li><a href="#one" class="active">About</a></li>
						<li><a href="#three">My Posts</a></li>
						<li><a href="#four">Contact</a></li>
					</ul>
				</nav>
				<footer>
					<ul class="icons">
						<li><a href="#" class="icon fa-twitter"><span class="label">Twitter</span></a></li>
						<li><a href="#" class="icon fa-facebook"><span class="label">Facebook</span></a></li>
						<li><a href="#" class="icon fa-instagram"><span class="label">Instagram</span></a></li>
						<li><a href="#" class="icon fa-github"><span class="label">Github</span></a></li>
						<li><a href="#" class="icon fa-envelope"><span class="label">Email</span></a></li>
					</ul>
				</footer>
			</section>

		<!-- Wrapper -->
			<div id="wrapper">

				<!-- Main -->
					<div id="main">

						<!-- One -->
							<section id="one">
								<div class="container">
									<header class="major">
										<h2>{{ $bloginfo['blogname'] }}</h2>
										<p>{{ $bloginfo['bloginfo'] }}@if (Auth::id() === $bloginfo['user_id'])
					<small><i class="fa fa-pencil"></i></small>
				@endif</p>
									</header>
								</div>
							</section>

						<!-- Three -->
							<section id="three">
								<div class="container">
									<h3>My Posts</h3>
									<div class="features">
									@if (count($posts) === 0)
										{{-- expr --}}
									
										<article>
											<div class="inner">
												<h4>No posts found</h4>
												<p>Sorry but this silo does not have any posts.</p>
											</div>
					@if (Auth::id() === $bloginfo['user_id'])
				<ul class="actions">
					<li><a href="{{ url('new') }}" class="button ion-ios-compose">New Post</a></li>
				</ul>
			@endif

										</article>
									@else
										<article>
											<div class="inner">
												<h4>{{ $post['title'] }}</h4>
												<p>{!! $post['post'] !!}</p>
											</div>
                                            @if (!Auth::guest())
                                                <button class="cbutton cbutton--effect-milan" onclick="like">
                                                    <i class="cbutton__icon fa fa-fw fa-heart"></i>
                                                    <span class="cbutton__text">Favorite</span>
                                                </button>
												<span>{{ $post->likeCount }}</span>
                                                <script>
                                                    var like = function(){
                                                        //Like The Post
                                                        <?php if($post->liked()) $post->unlike(); else $post->like(); ?>
                                                    }
                                                </script>
                                            @endif
                                            <div id="disqus_thread"></div>
							<script type="text/javascript">
							    /* * * CONFIGURATION VARIABLES * * */
							    var disqus_shortname = 'cornchat';
							    
							    /* * * DON'T EDIT BELOW THIS LINE * * */
							    (function() {
							        var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
							        dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
							        (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
							    })();
							</script>
							<noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript" rel="nofollow">comments powered by Disqus.</a></noscript>
										</article>
			@if (Auth::id() === $bloginfo['user_id'])
				<ul class="actions">
					<li><a href="{{ url('new') }}" class="button ion-ios-compose">New Post</a></li>
				</ul>
			@endif

									@endif
									</div>
								</div>
							</section>

						<!-- Four -->
							<section id="four">
								<div class="container">
									<h3>Contact Me</h3>
									<form method="post" action="#">
										<div class="row uniform">
											<div class="6u 12u(xsmall)"><input type="text" name="name" id="name" placeholder="Name" /></div>
											<div class="6u 12u(xsmall)"><input type="email" name="email" id="email" placeholder="Email" /></div>
										</div>
										<div class="row uniform">
											<div class="12u"><input type="text" name="subject" id="subject" placeholder="Subject" /></div>
										</div>
										<div class="row uniform">
											<div class="12u"><textarea name="message" id="message" placeholder="Message" rows="6"></textarea></div>
										</div>
										<div class="row uniform">
											<div class="12u">
												<ul class="actions">
													<li><input type="submit" class="special" value="Send Message" /></li>
													<li><input type="reset" value="Reset Form" /></li>
												</ul>
											</div>
										</div>
									</form>
								</div>
							</section>

					</div>

				<!-- Footer -->
					<section id="footer">
						<div class="container">
							<ul class="copyright">
								<li>&copy; CornChat</li><li>Design: <a href="http://html5up.net">HTML5 UP</a></li>
							</ul>
						</div>
					</section>

			</div>

		<!-- Scripts -->
			<script src="{{ ('themes/theme-2/assets/js/jquery.min.js') }}"></script>
			<script src="{{ ('themes/theme-2/assets/js/jquery.scrollzer.min.js') }}"></script>
			<script src="{{ ('themes/theme-2/assets/js/jquery.scrolly.min.js') }}"></script>
			<script src="{{ ('themes/theme-2/assets/js/skel.min.js') }}"></script>
			<script src="{{ ('themes/theme-2/assets/js/util.js') }}"></script>
			<!--[if lte IE 8]><script src="{{ ('themes/theme-2/assets/js/ie/respond.min.js') }}"></script><![endif]-->
			<script src="{{ ('themes/theme-2/assets/js/main.js') }}"></script>
<script type="text/javascript">
    /* * * CONFIGURATION VARIABLES * * */
    var disqus_shortname = 'cornchat';
    
    /* * * DON'T EDIT BELOW THIS LINE * * */
    (function () {
        var s = document.createElement('script'); s.async = true;
        s.type = 'text/javascript';
        s.src = '//' + disqus_shortname + '.disqus.com/count.js';
        (document.getElementsByTagName('HEAD')[0] || document.getElementsByTagName('BODY')[0]).appendChild(s);
    }());
</script>
	</body>
</html>