<?php $article = \App\BlogPost::where('id', $post['id'])->first() ?>
<?php function getUrl() {
  $url  = @( $_SERVER["HTTPS"] != 'on' ) ? 'http://'.$_SERVER["SERVER_NAME"] :  'https://'.$_SERVER["SERVER_NAME"];
  $url .= ( $_SERVER["SERVER_PORT"] !== 80 ) ? ":".$_SERVER["SERVER_PORT"] : "";
  $url .= $_SERVER["REQUEST_URI"];
  return $url;
} ?>
<!DOCTYPE html>
<html>
<head>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
	<title>{{ $post['title'] }} | LivOnAir</title>
	<meta charset="utf-8" />
	<meta http-equiv="refresh" content="300">
	<meta property="og:title" content="{{ $post['title'] }}" />
	<meta property="og:site_name" content="LivOnAir" />
	<meta property="og:url" content="http://{{ $post['blogname'] }}.livonair.com/" />
	<meta name="twitter:card" content="summary" />
	<meta name="twitter:site" content="@brainbuzzerme" />
	<meta name="twitter:title" content="{{ $post['title'] }}" />
	<link rel="stylesheet" href="https://storage.googleapis.com/code.getmdl.io/1.0.0/material.yellow-red.min.css">
	<script src="https://storage.googleapis.com/code.getmdl.io/1.0.0/material.min.js"></script>
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/blog-new.css') }}">
	<script type="text/javascript">var switchTo5x=true;</script>
	<script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
	<script type="text/javascript">stLight.options({publisher: "06f070fa-cc4f-41be-80bd-c11f333967a8", doNotHash: false, doNotCopy: false, hashAddressBar: false});</script>
</head>

<body>
<div class="demo-layout mdl-layout mdl-layout--fixed-header mdl-js-layout mdl-color--grey-100">
      <header class="demo-header mdl-layout__header mdl-layout__header--scroll mdl-color--grey-100 mdl-color-text--grey-800">
        <div class="mdl-layout__header-row">
          <span class="mdl-layout-title">{{ $user['name'] }}</span>
          <div class="mdl-layout-spacer"></div>
          <div class="mdl-textfield mdl-js-textfield mdl-textfield--expandable">
            <label class="mdl-button mdl-js-button mdl-button--icon" for="search">
              <i class="material-icons">search</i>
            </label>
            <div class="mdl-textfield__expandable-holder">
              <input class="mdl-textfield__input" type="text" id="search" />
              <label class="mdl-textfield__label" for="search">Enter your query...</label>
            </div>
          </div>
        </div>
      </header>
      <div class="demo-ribbon"></div>
      <main class="demo-main mdl-layout__content">
        <div class="demo-container mdl-grid">
          <div class="mdl-cell mdl-cell--2-col mdl-cell--hide-tablet mdl-cell--hide-phone"></div>
          <div class="demo-content mdl-color--white mdl-shadow--4dp content mdl-color-text--grey-800 mdl-cell mdl-cell--8-col">
            <div class="demo-crumbs mdl-color-text--grey-500">
              <time>{{ date('F d, Y', strtotime($post['created_at'])) }}</time>
            </div>
            <h3>{{ $post['title'] }}</h3><hr/>
              <p>{!! $post['post'] !!}</p><br/><br/>
              <hr/>
            	<span class='st_facebook_large' displayText='Facebook'></span>
				<span class='st_twitter_large' displayText='Tweet'></span>
				<span class='st_linkedin_large' displayText='LinkedIn'></span>
				<span class='st_pinterest_large' displayText='Pinterest'></span>
				<span class='st_wordpress_large' displayText='WordPress'></span>
				<span class='st_tumblr_large' displayText='Tumblr'></span>
				<span class='st_googleplus_large' displayText='Google +'></span>
				<span class='st_evernote_large' displayText='Evernote'></span>
				<span class='st_flipboard_large' displayText='Flipboard'></span>
				<span class='st_stumbleupon_large' displayText='StumbleUpon'></span>
				<span class='st_google_bmarks_large' displayText='Bookmarks'></span>
				<span class='st_fblike_large' displayText='Facebook Like'></span><br/>
						Total Likes: 
						@if(isset($article->likeCount)){{ $article->likeCount ? $article->likeCount : "N/A" }}@endif
						Tags: {{ $post['category'] }}
              							<form method="post" action="{{ url('post/comment') }}" data-remote>
								<input type="hidden" name="_token" value="{{ csrf_token() }}">
								<input type="hidden" name="_postId" value="{{ $post['id'] }}">	  						
								@if(\Auth::guest())
									<div class="row">
										<div class="mdl-cell mdl-cell--6-col">
							<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
							    <input class="mdl-textfield__input" type="email" required id="email" name="email" />
							    <label class="mdl-textfield__label" for="email">Email</label>
  							</div>
		    </div>
							<div class="mdl-cell mdl-cell--6-col">
							<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
							    <input class="mdl-textfield__input" type="text"required id="name" name="name" pattern="[A-Z,a-z, ]*" />
							    <label class="mdl-textfield__label" for="name">Name</label>
  							</div>
									    </div>
									</div>
							  @endif
								  <div class="mdl-textfield mdl-js-textfield textfield-demo">
    								<textarea class="mdl-textfield__input" type="text" required rows= "3" placeholder="Hi {{ $user['name'] }} ..." id="comment" name="comment"></textarea>
  								  </div>
								<input class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect" type="submit" value="Submit">
							</form>
							<hr/>
							<div class="all">
								@foreach ($comments as $comment)
									<div class="mdl-card mdl-shadow--8dp wide">
									  <div class="mdl-card__title">
									    <h2 class="mdl-card__title-text">{{ $comment->name }}</h2>
									  </div>
									  <div class="mdl-card__supporting-text">
									    {{ $comment->comment }}
									  </div>
									</div><br/></br/>
								@endforeach
							</div>
          </div>
        </div>
      </main>
    </div>


		<div class="copyright">&copy; {{ $user['name'] }} &nbsp;&nbsp;&#124; &nbsp;&nbsp;Hosted By LivOnAir</div>
	</div>
	<script type="text/javascript" src="{{ asset('js/form.read.js') }}"></script>
	@if(isset($bloginfo['analytics']))
		<script type="text/javascript">
			{!! $bloginfo['analytics'] !!}
		</script>
	@endif
		</div>
	  </main>
	</div>
@if(\Auth::id() === $user['id'])
	<a class="fab mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect" href="{{ url('/post/' . $post['slug'] . '/delete') }}">
		<i class="material-icons">delete</i>
	</a>
	<a class="fab-admin mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect" href="{{ url('/post/' . $post['slug'] . '/edit') }}">
		<i class="material-icons">edit</i>
	</a>
@endif
</body>
</html>
    