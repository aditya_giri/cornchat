<html>
	<head>
		<link href='//fonts.googleapis.com/css?family=Lato:100' rel='stylesheet' type='text/css'>
		<link rel="stylesheet" type="text/css" href="{{ asset('/css/ionicons.css') }}">
		<style>
			body {
				margin: 0;
				padding: 0;
				width: 100%;
				height: 100%;
				color: #ecf0f1;
				background-color: #2c3e50;
				display: table;
				font-weight: 100;
				font-family: 'Lato';
			}

			.container {
				text-align: center;
				display: table-cell;
				vertical-align: middle;
			}

			.content {
				text-align: center;
				display: inline-block;
			}

			.title {
				font-size: 72px;
				margin-bottom: 40px;
			}
			.low {
				font-size: 36px;
			}
		</style>
	</head>
	<body>
		<div class="container">
			<div class="content">
				<div class="title"><i class="ion-ios-cog-outline"></i></div>
				<div class="low">Be right back.</div>
			</div>
		</div>
	</body>
</html>
