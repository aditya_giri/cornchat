<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" type="text/css" href="{{ asset('/css/github/bootstrap.css') }}">
	<title>{{ $array['login'] }} - Github | CornChat</title>
</head>
<body>
	<nav class="navbar navbar-default" role="navigation">
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="#">CornChat</a>
		</div>
	
		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse navbar-ex1-collapse">
			<ul class="nav navbar-nav">
				<li class="active"><a href="#">Followers <span class="badge">{{ $array['followers'] }}</span></a></li>
				<li><a href="#">Following <span class="badge">{{ $array['following'] }}</span></a></li>
				<li><a href="#">Repositories <span class="badge">{{ $array['public_repos'] }}</span></a></li>
			</ul>
			<ul class="nav navbar-nav navbar-right">
				<img src="{{ $array['avatar_url'] }}" class="img-circle" width="50" alt="Profile Picture">
			</ul>
		</div><!-- /.navbar-collapse -->
	</nav>
	<div class="container">
		<div class="row">
			<div class="col-md-4">
				<div class="panel panel-default">
					<div class="panel-body">
					    <img src="{{ $array['avatar_url'] }}" class="img-circle img-thumbnail img-responsive" alt="Profile Picture" />
						<h3>{{ $array['login'] }}</h3>
					</div>
				</div>
			</div>
			<div class="col-md-8">
				@foreach ($repo as $r)
					<div class="well well-lg">
						<h4>{{ $r['name'] }}</h4>
						<span>Forks: {{ $r['forks'] }}, Stars: {{ $r['stargazers_count'] }}, Watching: {{ $r['watchers_count'] }}</span>
					</div>
				@endforeach
			</div>
		</div>
	</div>
</body>
</html>