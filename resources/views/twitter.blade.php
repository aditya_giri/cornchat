<!DOCTYPE html>
<html>
<head>
  <link rel="apple-touch-icon" sizes="57x57" href="{{ asset('favicons/apple-icon-57x57.png') }}">
<link rel="apple-touch-icon" sizes="60x60" href="{{ asset('favicons/apple-icon-60x60.png') }}">
<link rel="apple-touch-icon" sizes="72x72" href="{{ asset('favicons/apple-icon-72x72.png') }}">
<link rel="apple-touch-icon" sizes="76x76" href="{{ asset('favicons/apple-icon-76x76.png') }}">
<link rel="apple-touch-icon" sizes="114x114" href="{{ asset('favicons/apple-icon-114x114.png') }}">
<link rel="apple-touch-icon" sizes="120x120" href="{{ asset('favicons/apple-icon-120x120.png') }}">
<link rel="apple-touch-icon" sizes="144x144" href="{{ asset('favicons/apple-icon-144x144.png') }}">
<link rel="apple-touch-icon" sizes="152x152" href="{{ asset('favicons/apple-icon-152x152.png') }}">
<link rel="apple-touch-icon" sizes="180x180" href="{{ asset('favicons/apple-icon-180x180.png') }}">
<link rel="icon" type="image/png" sizes="192x192"  href="{{ asset('favicons/android-icon-192x192.png') }}">
<link rel="icon" type="image/png" sizes="32x32" href="{{ asset('favicons/favicon-32x32.png') }}">
<link rel="icon" type="image/png" sizes="96x96" href="{{ asset('favicons/favicon-96x96.png') }}">
<link rel="icon" type="image/png" sizes="16x16" href="{{ asset('favicons/favicon-16x16.png') }}">
<link rel="manifest" href="{{asset('favicons/manifest.json')}}">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="{{ asset('favicons/ms-icon-144x144.png') }}">
<meta name="theme-color" content="#ffffff">
  <meta name="description" content="LivOnAir ReImagines your social networking life and puts all the content in just one tab including weather, movies, songs, twitter feed and your blogging.">
  <meta name="keywords" content="livonair, twitter, livonair.com, livonair twitter, livonair social network, livonair aditya, aditya, aditya giri">
  <link rel="stylesheet" type="text/css" href="{{ asset('/css/ionicons.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('profile/css/style.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ asset('css/global.css') }}">
	<title>@yield('title') | LivOnAir</title>
	<script type="text/javascript" src="{{ asset('profile/js/webfont.js') }}"></script>
	<script type="text/javascript">
	    WebFont.load({
		  google: {
		    families: ["Droid Sans:400,700","Varela Round:400","Montserrat:400,700"]
		  }
		});
	</script>
	<script type="text/javascript" src="{{ asset('profile/js/modernizr.js') }}"></script>
  @yield('css')
</head>
<body>
  <nav>
            <ul>
                <li>
                    <a href="{{ url('twitter') }}">
                        <i class="fa ion-social-twitter fa-2x"></i>

                    </a>
                  
                </li>
                <li class="has-subnav">
                    <a href="{{ url('weather') }}">
                        <i class="fa ion-ios-cloud fa-2x"></i>

                    </a>
                    
                </li>
                <li class="has-subnav">
                    <a href="{{ url('news') }}">
                       <i class="fa ion-ios-paper fa-2x"></i>

                    </a>
                    
                </li>
                <li class="has-subnav">
                    <a href="{{ url('songs') }}">
                       <i class="fa ion-ios-musical-notes fa-2x"></i>
                    </a>
                   
                </li>
                <li>
                    <a href="{{ url('movie') }}">
                        <i class="fa ion-ios-film fa-2x"></i>
                    </a>
                </li>
                <li>
                    <?php $user = \App\User::find(Auth::id()); ?>
                    @if ($user['blogname'] !== null)
                        <a href="http://{{ $user['blogname'] }}.livonair.com">
                    @else
                        <a href="{{ url('newsilo') }}">
                    @endif
                    
                        <i class="fa fa-terminal fa-2x"></i>

                    </a>
                  
                </li>
            </ul>

            <ul class="logout">
              <li>
                   <a href="{{ url('help') }}">
                         <i class="fa ion-ios-help fa-2x"></i>
                    </a>
                </li>
                <li>
                   <a href="{{ url('/auth/logout') }}">
                         <i class="fa fa-power-off fa-2x"></i>
                    </a>
                </li>  
            </ul>
        </nav>
  <div class="container">
    <div class="w-clearfix info-block">
      <a class="header-link" href="#">Followers: {{ $array['followers_count'] }}</a><a class="header-link" href="#">Following: {{ $array['friends_count'] }}</a><a class="header-link" href="#tweets">Tweets</a>
    </div>
      @yield('user-info')
      @yield('user-tweets')
    </div>
  </div>

  <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
  <script type="text/javascript" src="{{ asset('profile/js/custom.js') }}"></script>
  <!--[if lte IE 9]><script src="//cdnjs.cloudflare.com/ajax/libs/placeholders/3.0.2/placeholders.min.js"></script><![endif]-->
  <script type="text/javascript">
( function() {
    CHITIKA = window.CHITIKA ? window.CHITIKA : { 'units' : [] };
    CHITIKA.publisher = 'cornchat';
    var s = document.createElement('script');
    s.src = '//cdn.chitika.net/getads.js';
    try { document.getElementsByTagName('head')[0].appendChild(s); } catch(e) { document.write(s.outerHTML); }
}());
</script>
  <script type="text/javascript" src="{{ asset('js/jquery.js') }}"></script>
  <script type="text/javascript">
  var menu = $('.ion-android-menu');
  var main-menu = $('.main-menu');
  menu.click(function(e){
    if (main-menu.hasClass('expanded')) {
        slidePageOut();
        main-menu.removeClass('expanded');
    }else{
        slidePageIn();
        main-menu.addClass('expanded');
    };
  });
  function slidePageIn(){
     $('.container').animate({
      left: '350px'
     }, 400, 'easeOutBack'); 
     $('.up-menu').animate({
      left: '350px'
     }, 400, 'easeOutBack'); 
  }
  function slidePageOut(){
     $(".main-menu").removeClass("expanded");
     $('.container').animate({
      left: '0px'
     }, 400, 'easeOutQuint'); 
     $('.up-menu').animate({
      left: '0px'
     }, 400, 'easeOutQuint'); 
  }
  </script>
  <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-63818132-1', 'auto');
  ga('send', 'pageview');

</script>
</body>
</html>