@if (Auth::guest())
    <?php header("Location: home"); die(); ?>
@else


    @extends('app')

    @section('title')
        Settings
    @stop

    @section('css')
        <link rel="stylesheet" type="text/css" href="css/components.css">
        <style>
          body{
            background-color: #9E9E9E;
          }
        </style>
    @stop

    @section('content')
        <div class="container">
            <form>
            Your country please: 
                <select class="form-control">
                    @foreach ($countries as $country)
                        <option>{{ $country }}</option>
                    @endforeach 
                </select>
            </form>
        </div>
    @stop
    @section('script')
        <script src="js/modernizr.min.js"></script>
        <script src="js/svgcheckbx.js"></script>
    @stop




@endif