@extends('app')

@section('title')
	Home
@stop

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">

			@if ( Auth::user()->twitter == '0' )
				<div>
					Might be you are wanted to login with twitter... Here's that...
					<a href="{{ url('twitter/register') }}" class="mui-btn mui-btn-primary">Login With Twitter</a>
				</div>
			@else
				<?php $user = \App\Twitter::where('user_id', Auth::user()->id)->first() ?>
				<?php $request = array('token' => $user->token, 'secret' => $user->token_secret); ?>
				<?php \Thujohn\Twitter\Facades\Twitter::reconfig($request)?>
				<?php $timeline = Twitter::getHomeTimeline(['count' => 300, 'format' => 'json']); ?>
				<?php $array = json_decode($timeline) ?>
				<div class="grid js-masonry" data-masonry-options='{ "itemSelector": ".grid-item", "columWidth": 400 }'>
				@foreach ($array as $tweet)
					@if (preg_match('/RT\ (@[a-zA-Z]+):/', $tweet->text) && isset($tweet->retweeted_status))
					<div class="box-tweet grid-item" id="{{ $tweet->id }}">
						<div class="list-group-item">
							<div class="blur"><img src="https://g.twimg.com/dev/documentation/image/retweet.png">Originally Retweeted by {{ $tweet->user->name }}</div>
							<div class="row-picture">
								<img class="circle" src="{{ $tweet->retweeted_status->user->profile_image_url }}" alt="Avatar">
							</div>
							<div class="row-content">
								<?php $media_array = json_decode(json_encode($tweet->entities), true) ?>
								<h4 class="list-group-item-heading">{!! Twitter::linkify($tweet->retweeted_status->text)!!}</h4>
									@foreach($media_array as $entity)
									        @if(isset($entity[0]['media_url']))
									            <img class="img-responsive" src="{{ $entity[0]['media_url'] }}"></img>
									        @endif
									@endforeach
								<p class="list-group-item-text">posted by <a target="_self" href="http://livonair.com/@<?= $tweet->retweeted_status->user->screen_name ?>">{{ $tweet->retweeted_status->user->name }}</a>↗ {{ Twitter::ago($tweet->retweeted_status->created_at) }}</p>
								<div class="icons">
									@if ($tweet->favorited == true)
										<a href="{{ url('twitter/refavorite?tweet=' . $tweet->id) }}"><img src="https://g.twimg.com/dev/documentation/image/favorite_on.png"></a>
									@else
										<a href="{{ url('twitter/favorite?tweet=' . $tweet->id) }}"><img src="https://g.twimg.com/dev/documentation/image/favorite.png"></a>
									@endif
									<i class="icon ion-ios-undo"></i>
								@if ($tweet->retweeted == false)
									<a href="{{ url('twitter/rt?id=' . $tweet->id) }}"><img src="https://g.twimg.com/dev/documentation/image/retweet.png"></a>
								@else
									<a href="{{ url('twitter/rt?id=' . $tweet->id) }}"><img src="https://g.twimg.com/dev/documentation/image/retweet_on.png"></a>
								@endif
								</div>
							</div>
						</div>
					</div>						
					@else
					<div class="box-tweet grid-item" id="{{ $tweet->id }}">
						<div class="list-group-item">
							<div class="row-picture">
								<img class="circle" src="{{ $tweet->user->profile_image_url }}" alt="Avatar">
							</div>
							<div class="row-content">
								<?php $media_array = json_decode(json_encode($tweet->entities), true) ?>
								<h4 class="list-group-item-heading">{!! Twitter::linkify($tweet->text) !!}</h4>
									@foreach($media_array as $entity)
									        @if(isset($entity[0]['media_url']))
									            <img class="img-responsive" src="{{ $entity[0]['media_url'] }}"></img>
									        @endif
									@endforeach
								<p class="list-group-item-text">posted by <a target="_self" href="http://livonair.com/@<?= $tweet->user->screen_name ?>">{{ $tweet->user->name }}</a>↗ {{ Twitter::ago($tweet->created_at) }}</p>
								<div class="icons">
									@if ($tweet->favorited == true)
										<a href="{{ url('twitter/refavorite?tweet=' . $tweet->id) }}"><img src="https://g.twimg.com/dev/documentation/image/favorite_on.png"></a>
									@else
										<a href="{{ url('twitter/favorite?tweet=' . $tweet->id) }}"><img src="https://g.twimg.com/dev/documentation/image/favorite.png"></a>
									@endif
									<img src="https://g.twimg.com/dev/documentation/image/reply.png">
									@if ($tweet->retweeted == false)
										<a href="{{ url('twitter/rt?id=' . $tweet->id) }}"><img src="https://g.twimg.com/dev/documentation/image/retweet.png"></a>
									@else
										<a href="{{ url('twitter/rt?id=' . $tweet->id) }}"><img src="https://g.twimg.com/dev/documentation/image/retweet_on.png"></a>
									@endif
								</div>
							</div>
						</div>
					</div>
					@endif
						{{-- <div class="list-group-separator"></div> --}}
				@endforeach
				</div>
			@endif			
		</div>
	</div>
</div>
@endsection

@section('script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/masonry/3.3.0/masonry.pkgd.min.js"></script>
@stop