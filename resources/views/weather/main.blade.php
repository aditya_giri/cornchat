<!DOCTYPE html>
<html>
<head>
    <link rel="apple-touch-icon" sizes="57x57" href="{{ asset('favicons/apple-icon-57x57.png') }}">
<link rel="apple-touch-icon" sizes="60x60" href="{{ asset('favicons/apple-icon-60x60.png') }}">
<link rel="apple-touch-icon" sizes="72x72" href="{{ asset('favicons/apple-icon-72x72.png') }}">
<link rel="apple-touch-icon" sizes="76x76" href="{{ asset('favicons/apple-icon-76x76.png') }}">
<link rel="apple-touch-icon" sizes="114x114" href="{{ asset('favicons/apple-icon-114x114.png') }}">
<link rel="apple-touch-icon" sizes="120x120" href="{{ asset('favicons/apple-icon-120x120.png') }}">
<link rel="apple-touch-icon" sizes="144x144" href="{{ asset('favicons/apple-icon-144x144.png') }}">
<link rel="apple-touch-icon" sizes="152x152" href="{{ asset('favicons/apple-icon-152x152.png') }}">
<link rel="apple-touch-icon" sizes="180x180" href="{{ asset('favicons/apple-icon-180x180.png') }}">
<link rel="icon" type="image/png" sizes="192x192"  href="{{ asset('favicons/android-icon-192x192.png') }}">
<link rel="icon" type="image/png" sizes="32x32" href="{{ asset('favicons/favicon-32x32.png') }}">
<link rel="icon" type="image/png" sizes="96x96" href="{{ asset('favicons/favicon-96x96.png') }}">
<link rel="icon" type="image/png" sizes="16x16" href="{{ asset('favicons/favicon-16x16.png') }}">
<link rel="manifest" href="{{asset('favicons/manifest.json')}}">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="{{ asset('favicons/ms-icon-144x144.png') }}">
<meta name="theme-color" content="#ffffff">
  <meta name="description" content="LivOnAir ReImagines your social networking life and puts all the content in just one tab including weather, movies, songs, twitter feed and your blogging.">
  <meta name="keywords" content="livonair, twitter, livonair.com, livonair twitter, livonair social network, livonair aditya, aditya, aditya giri">
  <link rel="stylesheet" type="text/css" href="{{ asset('/css/weather.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ asset('/css/ionicons.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ asset('/css/normalize.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ asset('/css/gridism.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ asset('/css/global.css') }}">

  <title>Weather | LivOnAir</title>
	<link href='http://fonts.googleapis.com/css?family=Titillium+Web' rel='stylesheet' type='text/css'>
	<meta name="viewport" content="width=device-width,initial-scale=1">
</head>
<body>

		<div class="area"></div><nav class="main-menu">
            <ul>
                <li>
                    <a href="{{ url('twitter') }}">
                        <i class="fa ion-social-twitter fa-2x"></i>
                        <span class="nav-text">
                            Twitter
                        </span>
                    </a>
                  
                </li>
                <li class="has-subnav">
                    <a href="{{ url('weather') }}">
                        <i class="fa ion-ios-cloud fa-2x"></i>
                        <span class="nav-text">
                            Weather
                        </span>
                    </a>
                    
                </li>
                <li class="has-subnav">
                    <a href="{{ url('news') }}">
                       <i class="fa ion-ios-paper fa-2x"></i>
                        <span class="nav-text">
                            News
                        </span>
                    </a>
                    
                </li>
                <li class="has-subnav">
                    <a href="{{ url('songs') }}">
                       <i class="fa ion-ios-musical-notes fa-2x"></i>
                        <span class="nav-text">
                            Songs
                        </span>
                    </a>
                   
                </li>
                <li>
                    <a href="{{ url('movie') }}">
                        <i class="fa ion-ios-film fa-2x"></i>
                        <span class="nav-text">
                            Movie
                        </span>
                    </a>
                </li>
                <li>
                    <?php $user = \App\User::find(Auth::id()); ?>
                    @if ($user['blogname'] !== null)
                        <a href="{{ $user['blogname'] }}.livonair.com">
                    @else
                        <a href="{{ url('newsilo') }}">
                    @endif
                    
                        <i class="fa fa-connectdevelop fa-2x"></i>
                        <span class="nav-text">
                            Your Silo
                        </span>
                    </a>
                  
                </li>
            </ul>

            <ul class="logout">
            	<li>
                   <a href="{{ url('help') }}">
                         <i class="fa ion-ios-help fa-2x"></i>
                        <span class="nav-text">
                            Support
                        </span>
                    </a>
                </li>
                <li>
                   <a href="{{ url('/auth/logout') }}">
                         <i class="fa fa-power-off fa-2x"></i>
                        <span class="nav-text">
                            Logout
                        </span>
                    </a>
                </li>  
            </ul>
        </nav>
        <ul class="up-menu">
            <li><i class="ionicons ion-android-menu"></i></li>
            <li><i class="ionicons ion-ios-search-strong"></i>
              <form action="search"><input type="text" placeholder="Search for blogs" name="q"></form>
            </li>
            <li>{{ $user['name'] }}</li>
        </ul>
  <div class="container">
    <div class="center-search">
        <center>
            	<form>
            		<input placeholder="City Name Please" type="text" name="city"></input>
            		<button type="submit" class="ion-ios-search btn"></button>
            	</form>
        </center>
    </div>
    
    	@if (isset($response))
    		<div class="result">
    			<?php $array = $response->json() ?>
				<div class="grid">
					<div class="unit half">Name: </div>
					<div class="unit half">{{ $array['name'] }}</div>
				</div>
				<div class="grid">
					<div class="unit half">Weather: </div>
					<div class="unit half">
						@foreach ($array['weather'] as $element)
							<div class="grid">
								<img src="http://openweathermap.org/img/w/{{ $element['icon'] }}.png">
								<span> {{ $element['description'] }}</span>
							</div>
						@endforeach
					</div>
				</div>
				<div class="grid">
					<div class="unit half">Wind Speed: </div>
					<div class="unit half">
						{{ $array['wind']['speed'] }} m/s 
					</div>
				</div>
				<div class="grid">
					<div class="unit half">Humidity: </div>
					<div class="unit half">{{ $array['main']['humidity'] }}%</div>
				</div>
				<div class="grid">
					<div class="unit half">Pressure: </div>
					<div class="unit half">{{ $array['main']['pressure'] }} hpa</div>
				</div>
				<div class="grid">
					<div class="unit half">Temprature: </div>
					<div class="unit half">{{ $array['main']['temp'] }} °C</div>
				</div>
			</div>
    	@endif
        <script type="text/javascript">
  ( function() {
    if (window.CHITIKA === undefined) { window.CHITIKA = { 'units' : [] }; };
    var unit = {"calltype":"async[2]","publisher":"cornchat","width":728,"height":90,"sid":"Chitika Default"};
    var placement_id = window.CHITIKA.units.length;
    window.CHITIKA.units.push(unit);
    document.write('<div id="chitikaAdBlock-' + placement_id + '"></div>');
}());
</script>
<script type="text/javascript" src="//cdn.chitika.net/getads.js" async></script>
		<script type="text/javascript" src="http://srvpub.com/adServe/banners?tid=34498_50173_5&type=footer&size=728x90" ></script>    
  </div>
  <script src="{{ asset('js/classie.js') }}"></script>
  <script type="text/javascript" src="{{ asset('js/jquery.js') }}"></script>
  <script type="text/javascript">
  var menu = $('.ion-android-menu');
  var main-menu = $('.main-menu');
  menu.click(function(e){
    if (main-menu.hasClass('expanded')) {
        slidePageOut();
        main-menu.removeClass('expanded');
    }else{
        slidePageIn();
        main-menu.addClass('expanded');
    };
  });
  function slidePageIn(){
     $('.container').animate({
      left: '350px'
     }, 400, 'easeOutBack'); 
     $('.up-menu').animate({
      left: '350px'
     }, 400, 'easeOutBack'); 
  }
  function slidePageOut(){
     $(".main-menu").removeClass("expanded");
     $('.container').animate({
      left: '0px'
     }, 400, 'easeOutQuint'); 
     $('.up-menu').animate({
      left: '0px'
     }, 400, 'easeOutQuint'); 
  }
  </script>
  <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-63818132-1', 'auto');
  ga('send', 'pageview');

</script>
</body>
</html>