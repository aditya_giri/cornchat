@if ( ! Auth::guest() )
	<? \Redirect::to('twitter') ?>
@endif
<!DOCTYPE html>
<html>
<head>
<meta name="google-site-verification" content="vS51PqZR_PJAItuBvJwa0HTGCzKJ_lvMTOeTE-CvDPM" />	
	<title>LivOnAir | ReImagining social life</title>
	<meta name="description" content="LivOnAir ReImagines your social networking life and puts all the content in just one tab including weather, movies, songs, twitter feed and your blogging.">
	<meta name="keywords" content="livonair, twitter, livonair.com, livonair twitter, livonair social network, livonair aditya, aditya, aditya giri">
	<link rel="stylesheet" type="text/css" href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
	<link rel="stylesheet" type="text/css" href="css/normalize.css">
	<style type="text/css">
		@import url(http://fonts.googleapis.com/css?family=Lato:300);
		html { 
			background: url(img/bg.jpg) no-repeat center center fixed; 
			-webkit-background-size: cover;
			-moz-background-size: cover;
			-o-background-size: cover;
			background-size: cover;
			color: #FFF;
			font-family: 'Lato', sans-serif;
		}
		img {
			margin-top: 60px;
		}
		a {
			text-decoration: none;
			color: #FFF;
		}
		.on-right{
			float: right;
			margin-right: 10rem;
			display: block;
			height: 100vh;
			width: 480px;
		}
		.top-logo {
			margin-left: 30%;
		}
		.top-logo h2 {
			margin-left: -10%;
		}
		.source{
			justify-content: center;
			margin-left: 10%;
			width: 320px;
			display:block;
		}
		.btn {
			width: 20rem;
			border: 1px solid #eee;
			padding: 10px;
			background-color: #3498db;
			transition: background-color 0.2s linear;
			display: block;
		}
		.btn:hover {
			background-color: #2980b9;
		}
		.info {
			margin-left: 30%;
			color: #eee;
		}

		@media screen and (max-width : 480px) {
			.on-right{
				float: right;
				margin-right: 0;
			}
		}
	</style>
</head>
<body>
	<div class="on-right">
		<div class="top-logo">
			<img src="logo.svg" width="150">
			<h2>ReImagining social life</h2>
		</div>
		<br/>
		<div class="source">
			<a href="auth/login" class="btn">Login</a><br/>
			<a href="auth/register" class="btn">Register</a><br/>
		</div>
		<div class="info">
			<br/>Don't know anything <a href="about"><strong>about us?</strong></a>
		</div>
	</div>
	<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-63818132-1', 'auto');
  ga('send', 'pageview');

</script>
</body>
</html>