@if (Auth::guest() || Auth::id() !== $blog->user_id)
	<?php return \Redirect::to(url('/')) ?>
@endif
<!DOCTYPE html>
<html>
<head>
	<title>Admin | LivOnAir</title>
	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootswatch/3.3.5/darkly/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/admin.css') }}">
</head>
<body>
	<div class="container">
		<div class="inline">
			<div class="box grid-item">
				<h2>{{ $user->name }}</h2><br/>
				<p>{{ $blog->bloginfo }}</p>
				<a class="btn btn-primary" data-toggle="modal" href='#userinfo'>Edit This Info</a>
			</div>
			<div class="box grid-item">
				<a class="btn btn-primary" href="{{ url('admin/new') }}">New Post</a>
			</div>
			<div class="box grid-item">
				Want to edit even further?<br/>
				<a href="{{ url('admin/edit') }}" class="btn btn-primary">Edit Now</a>
			</div>
		</div>
		<canvas id="myChart" style="width: 100%; height: 400px;"></canvas>
	</div>



	<div class="modal fade" id="userinfo">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Edit User Info</h4>
				</div>
				<div class="modal-body">
					<form method="POST" action="{{ url('admin') }}" accept-charset="UTF-8" data-remote>
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
					    <div class="form-group">
					        <label for="name">Name</label>
					        <input class="form-control" required="required" value="{{ $user->name }}" name="name" type="text" id="name">
					    </div>

					    <div class="form-group">
					        <label for="tagline">Tagline</label>
					        <input class="form-control" required="required" name="tagline" type="text" value="{{ $blog->bloginfo }}" id="tagline">
					    </div>
						<div class="modal-footer">
							<div class="btn-group pull-right">
								<input class="btn btn-warning" type="reset" value="Reset">
								<input class="btn btn-success" type="submit" value="Add">
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
	<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/1.0.2/Chart.min.js"></script>
	<script type="text/javascript" src="{{ asset('js/admin.js') }}"></script>
	<script type="text/javascript">
		var msg = $.ajax({type: "GET", url: "api/json/analytics", async: false}).responseText;
		var ctx = $("#myChart").get(0).getContext("2d");
		var dates = new Array();
	    var val = new Array();

		var obj = jQuery.parseJSON(msg);
		$.each(obj, function(key,value) {
		  dates.push(value.date);
		  val.push(value.pageviews);
		}); 

		var myLineChart = new Chart(ctx).Line({
		    labels: dates,
		    datasets: [
		        {
		            label: "PageLoads",
		            fillColor: "rgba(220,220,220,0.2)",
		            strokeColor: "rgba(220,220,220,1)",
		            pointColor: "rgba(220,220,220,1)",
		            pointStrokeColor: "#fff",
		            pointHighlightFill: "#fff",
		            pointHighlightStroke: "rgba(220,220,220,1)",
		            data: val
		        }
		    ]
		});
	</script>
</body>
</html>