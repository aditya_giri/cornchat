@extends('twitter')

@section('css')
	<style>
		.vertical-nav {
			background-color: {{ $array['profile_sidebar_fill_color'] }};
		}
	</style>
@stop

@section('title')
	{{ $array['name'] }}
@stop
@section('user-info')
<div class="main-content" style="background-image: url('{{ $array['profile_background_image_url_https'] }}'); background-attachment: fixed; background-repeat: no-repeat; background-position: bottom; background-size: cover; background-color: #{{ $array['profile_background_color'] }}">

	<div class="user-info" style="background-color: #{{ $array['profile_background_color'] }}">
		<img src="{{ $image }}">
		<div class="user-info-bio">
			<h1>Bio</h1>
			<span>Name: {{ $array['name'] }}</span><br>
			<span>Description: {{ $array['description'] }}</span><br/>
			<br>
			<span>Official Twitter Link : <a href="http://twitter.com/{{ $array['screen_name'] }}">{{ $array['screen_name'] }}</a></span>
        </div>
      </div>
@stop

@section('user-tweets')
<div id="tweets"></div>
	<center><script type="text/javascript" src="http://srvpub.com/adServe/banners?tid=34498_50173_3&size=728x90" ></script></center>
	<?php $chunk = array_chunk($tweets, 10) ?>
	@foreach ($chunk as $eachchunk)
		@foreach ($eachchunk as $tweet)
			<div class="w-clearfix post-text"><img class="img-thumbnail" src="{{ $image }}">
	        <p>{!! Twitter::linkify($tweet->text) !!}
	          <br>
	          <br>Posted by - <a href="http://local.dev:8888/twitter/{{ $tweet->user->screen_name }}">{{ $tweet->user->name }}</a> {{ Twitter::ago($tweet->created_at) }}</p>

	          <div>
					@if ($tweet->favorited == true)
						<a href="{{ url('twitter/refavorite?tweet=' . $tweet->id) }}"><i id="favorite{{ $tweet->id }}" class="icon ion-ios-star favorited"></i></a>
					@else
						<a href="{{ url('twitter/favorite?tweet=' . $tweet->id) }}"><i class="icon ion-ios-star"></i></a>
					@endif
					<i class="icon ion-ios-undo"></i>
					@if ($tweet->retweeted == false)
						<a href="{{ url('twitter/rt?id=' . $tweet->id) }}"><i class="icon ion-ios-refresh-outline"></i></a>
					@else
						<a href="{{ url('twitter/rt?id=' . $tweet->id) }}"><i class="icon ion-ios-refresh"></i></a>
					@endif
	          </div>
	      </div>
		@endforeach
	@endforeach
</div>      

@stop
    