@extends('twitter')

@section('title')
	{{ $array['name'] }}
@stop

@section('followers_no')
	{{ $array['followers_count'] }}
@stop

@section('following_no')
	{{ $array['friends_count'] }}
@stop

@section('tweet_no')
	{{ $array['statuses_count'] }}
@stop

@section('name')
	{{ $array['name'] }}
@stop

@section('img_nav')
	<img src="{{ $image }}" alt="profile picture" class="img-circle" width="64">
@stop

@section('content')
<pre><code>
	{{ var_dump($followers_json) }}
</code></pre>
@stop