@extends('app')

@section('title')
	Tweet Something
@stop

@section('css')
	
@stop

@section('content')
<div class="container">
	<form method="post" action="{{ url('post/twitter/post') }}">
		<div class="well well-lg">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			  <input type="text" class="form-control floating-label" name="tweet" placeholder="Tweet">
			<button type="submit" class="mui-btn mui-btn-primary">Tweet</button>
		</div>
	</form>
</div>
@stop

@section('script')
	
@stop