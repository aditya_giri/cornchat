@extends('public')

@section('title')
	Songs
@stop

@section('css')
	<link rel="stylesheet" type="text/css" href="{{ asset('/css/songs.css') }}">
@stop

@section('content')
<center>
	<img src="{{ asset('/img/itunes.png') }}" class="logo">
</center>
<hr>
<center>
		<form id="nl-form" class="nl-form">
	I want to have <input type="text" required value="" name="q" placeholder="some" data-subline="For example: <em>Bezubaan</em> or <em>Baby</em>"/> song.
	<div class="nl-submit-wrap">
		<button class="nl-submit" type="submit">Find a song</button>
	</div>
	<div class="nl-overlay"></div>
</form>
@if (isset($array))
<div class="results">
	<small>Found {{ $array['resultCount'] }} results.</small>
	@foreach ($array['results'] as $element)
		<div class="content">
			<div class="grid">
				<div class="unit one-third">
					<img src="{{ $element['artworkUrl100'] }}">
				</div>
				<div class="unit two-thirds">
					Track: {{ $element['trackName'] }}<br/>
					@if (isset($element['collectionName']))
						Collection: {{ $element['collectionName'] }}<br/>
					@endif
					Singer/s: {{ $element['artistName'] }}<br/>
					Release Date: {{ $element['releaseDate'] }}
				</div>
			</div>
			Grab this <i class="ion-ios-musical-notes"></i> on <a href="{{ $element['trackViewUrl'] }}">iTunes</a>@if (isset($element['trackPrice']))
				for {{ $element['trackPrice'] }}$.
			@endif 
			@if (isset($element['collectionPrice']))
				Or grab complete collection for {{ $element['collectionPrice'] }}$.
			@endif
		</div>
	@endforeach
</div>
</center>
@else

<script type="text/javascript">
  ( function() {
    if (window.CHITIKA === undefined) { window.CHITIKA = { 'units' : [] }; };
    var unit = {"calltype":"async[2]","publisher":"cornchat","width":550,"height":"auto","sid":"Chitika Default","fluidH":true};
    var placement_id = window.CHITIKA.units.length;
    window.CHITIKA.units.push(unit);
    document.write('<div id="chitikaAdBlock-' + placement_id + '"></div>');
}());
</script>
<script type="text/javascript" src="//cdn.chitika.net/getads.js" async></script>
@endif
@stop

@section('scripts')
	<script type="text/javascript" src="{{ asset('/js/nlform.js') }}"></script>
	<script type="text/javascript">
		var nlform = new NLForm( document.getElementById( 'nl-form' ) );
	</script>
@stop