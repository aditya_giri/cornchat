@extends('public')

@section('css')
	<link rel="stylesheet" type="text/css" href="{{ asset('/css/movie.css') }}">
@stop

@section('title')
	{{ $info['response']['title'] }}
@stop

@section('content')
	@foreach ($info as $element)
		@if (is_array($element))
			<div class="grid">
				<div class="unit half">
					<img src="{{ $element['poster']['url'] }}">
				</div>
				<div class="unit half">
					<h2>{{ $element['title'] }}</h2><br/>
					Description: {{ $element['description'] }}<br/>
				</div>
			</div>
			<script type="text/javascript">
  ( function() {
    if (window.CHITIKA === undefined) { window.CHITIKA = { 'units' : [] }; };
    var unit = {"calltype":"async[2]","publisher":"cornchat","width":468,"height":250,"sid":"Chitika Default"};
    var placement_id = window.CHITIKA.units.length;
    window.CHITIKA.units.push(unit);
    document.write('<div id="chitikaAdBlock-' + placement_id + '"></div>');
}());
</script>
<script type="text/javascript" src="//cdn.chitika.net/getads.js" async></script>
		@endif
	@endforeach
@stop