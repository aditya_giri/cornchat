@extends('public')

@section('css')
	<link rel="stylesheet" type="text/css" href="{{ asset('/css/movie.css') }}">
@stop

@section('title')
	Movies
@stop

@section('content')
	<center>
		<div class="title">
			Movies
		</div>
	</center>
	<hr>
	<center>
		<form>
			<h3>What movie would you like to search for?</h3><br/>
			<input type="text" placeholder="Movie Name"class="input" name="name">
			OR
			<input type="text" placeholder="Genre Name" class="input" name="genre">
			<button type="submit" class="btn-submit">Search</button>
		</form>
	</center>
	{{-- <pre><code>{{ print_r($genres) }}</code></pre> --}}
	@if ((!isset($_REQUEST['name']) || !isset($_REQUEST['genre'])) && isset($genres))
		<div class="row">
			@foreach ($genres['response'] as $genre)
				<div class="genre">
					<div class="grid">
						<div class="unit one-third">
							<img src="{{ $genre['images']['0']['url'] }}">
						</div>
						<div class="unit two-thirds">
							Name: {{ $genre['name'] }}<br/>
							Total Movies: {{ $genre['content_count'] }}<br/>
							Search For: <a href="{{ url('movie?genre=' . $genre['slug']) }}"> {{ $genre['slug'] }}</a>
						</div>
					</div>
				</div>
			@endforeach
		</div>
	@endif
	@if (isset($info))
		<div class="row">
		@foreach ($info['response'] as $movie)
			<div class="genre">
				<div class="grid">
					<div class="unit one-third">
						@foreach ($movie['artwork'] as $element)
							<img src="{{ $element['thumbs']['thumb_120p']['url'] }}">
						@endforeach
					</div>
					<div class="unit two-thirds">
						Title: {{ $movie['title'] }}<br/>
						Genre: @foreach ($movie['genres'] as $element)
							{{ $element['name'] }}
						@endforeach
						<br/><br/>
						<a href="{{ url('movie/' . $movie['id']) }}"><i class="ion-more"></i>Get more info</a>
					</div>
				</div>
			</div>
		@endforeach
		</div>
		<script type="text/javascript">
  ( function() {
    if (window.CHITIKA === undefined) { window.CHITIKA = { 'units' : [] }; };
    var unit = {"calltype":"async[2]","publisher":"cornchat","width":728,"height":90,"sid":"Chitika Default"};
    var placement_id = window.CHITIKA.units.length;
    window.CHITIKA.units.push(unit);
    document.write('<div id="chitikaAdBlock-' + placement_id + '"></div>');
}());
</script>
<script type="text/javascript" src="//cdn.chitika.net/getads.js" async></script>
	@endif
@stop