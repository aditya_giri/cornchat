@extends('public')

@section('content')
	@if ($blogs->count())
		@foreach ($blogs as $blog)
			<div class="well">
				{{ $blog->blogname }}<br/>
				{{ $blog->bloginfo }}
			</div>
		@endforeach
	@endif
@stop