<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="A front-end template that helps you build fast, modern mobile web apps.">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{ $category }} | {{ $user->name }}</title>

    <link rel="stylesheet" href="https://storage.googleapis.com/code.getmdl.io/1.0.0/material.indigo-pink.min.css">
    <script src="https://storage.googleapis.com/code.getmdl.io/1.0.0/material.min.js"></script>
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="{{ asset('css/cat.css') }}">
  </head>
  <body class="mdl-demo mdl-color--grey-100 mdl-color-text--grey-700 mdl-base">
    <div class="mdl-layout mdl-js-layout mdl-layout--fixed-header">
      <header class="mdl-layout__header mdl-layout__header--scroll mdl-color--primary">
        <div class="mdl-layout--large-screen-only mdl-layout__header-row">
        </div>
        <div class="mdl-layout--large-screen-only mdl-layout__header-row">
          <h3>{{ $category }} &mdash; {{ $user->name }}</h3>
        </div>
        <div class="mdl-layout--large-screen-only mdl-layout__header-row">
        </div>
        <div class="mdl-layout__tab-bar mdl-js-ripple-effect mdl-color--primary-dark">
          <button class="mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect mdl-button--colored mdl-shadow--4dp mdl-color--accent" id="add">
            <i class="material-icons">add</i>
          </button>
        </div>
      </header>
      <main class="mdl-layout__content">
        <div id="post">
            @foreach ($posts as $post)
            @if (strpos($post['post'], "<!--skipfromhere-->"))
              <?php $str = strpos($post->post, "<!--skipfromhere-->") ?>
            @else
              <?php $str = strlen($post->post) ?>
            @endif
           <section class="section--center mdl-grid mdl-grid--no-spacing mdl-shadow--2dp">
            <div class="mdl-card mdl-cell mdl-cell--12-col">
              <div class="mdl-card__supporting-text">
                <h4><a href="{{ url('/post/' . $post->slug) }}">{{ $post->title }}</a></h4>
                {!! substr($post['post'], 0, $str) !!}
              </div>
              <div class="mdl-card__actions">
                <a href="{{ url('/post/' . $post->slug) }}" class="mdl-button">Read More</a>
              </div>
            </div>
          </section>
          @endforeach
        </div>
      </main>
    </div>
  </body>
</html>