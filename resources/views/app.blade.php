<!DOCTYPE html>
<html lang="en">
<head>
<link rel="apple-touch-icon" sizes="57x57" href="{{ asset('favicons/apple-icon-57x57.png') }}">
<link rel="apple-touch-icon" sizes="60x60" href="{{ asset('favicons/apple-icon-60x60.png') }}">
<link rel="apple-touch-icon" sizes="72x72" href="{{ asset('favicons/apple-icon-72x72.png') }}">
<link rel="apple-touch-icon" sizes="76x76" href="{{ asset('favicons/apple-icon-76x76.png') }}">
<link rel="apple-touch-icon" sizes="114x114" href="{{ asset('favicons/apple-icon-114x114.png') }}">
<link rel="apple-touch-icon" sizes="120x120" href="{{ asset('favicons/apple-icon-120x120.png') }}">
<link rel="apple-touch-icon" sizes="144x144" href="{{ asset('favicons/apple-icon-144x144.png') }}">
<link rel="apple-touch-icon" sizes="152x152" href="{{ asset('favicons/apple-icon-152x152.png') }}">
<link rel="apple-touch-icon" sizes="180x180" href="{{ asset('favicons/apple-icon-180x180.png') }}">
<link rel="icon" type="image/png" sizes="192x192"  href="{{ asset('favicons/android-icon-192x192.png') }}">
<link rel="icon" type="image/png" sizes="32x32" href="{{ asset('favicons/favicon-32x32.png') }}">
<link rel="icon" type="image/png" sizes="96x96" href="{{ asset('favicons/favicon-96x96.png') }}">
<link rel="icon" type="image/png" sizes="16x16" href="{{ asset('favicons/favicon-16x16.png') }}">
<link rel="manifest" href="{{asset('favicons/manifest.json')}}">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="{{ asset('favicons/ms-icon-144x144.png') }}">
<meta name="theme-color" content="#ffffff">
  <meta name="description" content="LivOnAir ReImagines your social networking life and puts all the content in just one tab including weather, movies, songs, twitter feed and your blogging.">
  <meta name="keywords" content="livonair, twitter, livonair.com, livonair twitter, livonair social network, livonair aditya, aditya, aditya giri">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
  @yield('meta')
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>@yield('title') | LivOnAir</title>
	<script type="text/javascript" src="{{ asset('js/scrollReveal.js') }}"></script>
	<script type="text/javascript">window.sr = new scrollReveal();</script>
	<link rel="stylesheet" type="text/css" href="{{ asset('/css/ionicons.css') }}">
	<link href="{{ asset('/css/app.css') }}" rel="stylesheet">
	<link href="{{ asset('/css/material-fullpalette.min.css') }}" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="{{ asset('/css/ripples.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('/css/roboto.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('/css/global.css') }}">
	<!-- Fonts -->
	<link href='//fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>
	<!-- Load Material Design CSS from muicss.com -->
	<link href="//cdn.muicss.com/mui-0.0.10/css/mui.min.css" rel="stylesheet" type="text/css" />
    <script src="//cdn.muicss.com/mui-0.0.10/js/mui.min.js"></script>
	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
	@yield('css')
</head>
<body>
	<nav>
            <ul>
                <li>
                    <a href="{{ url('twitter') }}">
                        <i class="fa ion-social-twitter fa-2x"></i>

                    </a>
                  
                </li>
                <li class="has-subnav">
                    <a href="{{ url('weather') }}">
                        <i class="fa ion-ios-cloud fa-2x"></i>

                    </a>
                    
                </li>
                <li class="has-subnav">
                    <a href="{{ url('news') }}">
                       <i class="fa ion-ios-paper fa-2x"></i>

                    </a>
                    
                </li>
                <li class="has-subnav">
                    <a href="{{ url('songs') }}">
                       <i class="fa ion-ios-musical-notes fa-2x"></i>
                    </a>
                   
                </li>
                <li>
                    <a href="{{ url('movie') }}">
                        <i class="fa ion-ios-film fa-2x"></i>
                    </a>
                </li>
                <li>
                    <?php $user = \App\User::find(Auth::id()); ?>
                    @if ($user['blogname'] !== null)
                        <a href="http://{{ $user['blogname'] }}.livonair.com">
                    @else
                        <a href="{{ url('newsilo') }}">
                    @endif
                    
                        <i class="fa fa-terminal fa-2x"></i>

                    </a>
                  
                </li>
            </ul>

            <ul class="logout">
            	<li>
                   <a href="{{ url('help') }}">
                         <i class="fa ion-ios-help fa-2x"></i>
                    </a>
                </li>
                <li>
                   <a href="{{ url('/auth/logout') }}">
                         <i class="fa fa-power-off fa-2x"></i>
                    </a>
                </li>  
            </ul>
        </nav>
	@yield('content')

	<!-- Scripts -->
	<script type="text/javascript" src="{{ asset('/js/ripples.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('js/material.min.js') }}"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/noUiSlider/6.2.0/jquery.nouislider.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.1/js/bootstrap.min.js"></script>
	<script type="text/javascript">
        window.sr = new scrollReveal();
	</script>
  <script type="text/javascript" src="{{ asset('js/jquery.js') }}"></script>
  <script type="text/javascript">
  var menu = $('.ion-android-menu');
  var main-menu = $('.main-menu');
  menu.click(function(e){
    if (main-menu.hasClass('expanded')) {
        slidePageOut();
        main-menu.removeClass('expanded');
    }else{
        slidePageIn();
        main-menu.addClass('expanded');
    };
  });
  function slidePageIn(){
     $('.container').animate({
      left: '350px'
     }, 400, 'easeOutBack'); 
     $('.up-menu').animate({
      left: '350px'
     }, 400, 'easeOutBack'); 
  }
  function slidePageOut(){
     $(".main-menu").removeClass("expanded");
     $('.container').animate({
      left: '0px'
     }, 400, 'easeOutQuint'); 
     $('.up-menu').animate({
      left: '0px'
     }, 400, 'easeOutQuint'); 
  }
  </script>
	@yield('script')
</body>
</html>
