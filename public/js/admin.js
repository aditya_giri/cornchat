(function(){

	$('form[data-remote]').on('submit', function(e){
		
		e.preventDefault();

		var form = $(this);
		var method = form.find('input[name="_method"]').val() || 'POST';
		var url = form.prop('action')
		$.ajax({
			type: method,
			url: url,
			data: form.serialize(),
			success: function(){
				$('#userinfo').modal('hide');
				location.reload(false);
			}
		});

	});
	
})();