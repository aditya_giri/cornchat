<?php
    if(isset($_GET['a']) && isset($_GET['b'])){
        $a = $_GET['a'];
        $b = $_GET['b'];
        while($a%$b > -1){
            $q = intval($a / $b);
            echo $a . " = " . $q . " x " . $b . " + " . $a%$b . "<br/>";
            $r = $a%$b;
            $a = $b;
            $b = $r;
        }
        echo "<br/><br/><br/>So the greatest common devisor(GCD) is $a";
    }else{
?>

<html>
    <head>
        <title>Euclid's Division Algo</title>
    </head>
    <body>
        <form>
            <input type="number" name="a"/>
            <input type="number" name="b"/>
            <input type="submit"/>
        </form>
    </body>
</html>
<?php } ?>