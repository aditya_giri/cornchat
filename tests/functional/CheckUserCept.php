<?php 
$I = new FunctionalTester($scenario);
$I->wantTo('perform actions and see result');

$I->am('a member');
$I->amGoingTo('register');
$I->wantTo('Check if this ones can register me or not????');

$I->amOnPage('/auth/register');
$I->fillField('name', 'Aditya');
$I->fillField('blogname', 'aditya');
$I->fillField('email', 'adi@example.com');
$I->fillField('password', 'password');
$I->fillField('password_confirmation', 'password');
$I->click('submit');

$I->seeInDatabase('users', ['name' => 'Aditya', 'email' => 'adi@example.com']);

$I->amOnPage('/twitter');
$I->see('Aditya');
