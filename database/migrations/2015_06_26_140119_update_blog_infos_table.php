<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateBlogInfosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('blog_info', function(Blueprint $table){
            $table->string('title', 100);
            $table->string('color', 7);
            $table->string('copyright', 50);
            $table->longText('css');
            $table->string('override-css', 1)->default("F");
            $table->string('lastseen', 20);
            $table->integer('pageviews');
            $table->string('pagetitle');
            $table->text('analytics');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
