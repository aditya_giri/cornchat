<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserBlogsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('user_blogs_posts', function(Blueprint $table)
		{
            $table->increments('id');
            $table->integer('user_id');
            $table->string('blogname');
            $table->string('title');
            $table->mediumText('post');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('user_blogs_posts');
	}

}
