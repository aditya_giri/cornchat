<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlogInfosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('blog_info', function(Blueprint $table)
		{
			$table->increments('id');
            $table->string('blogname')->unique();
            $table->string('bloginfo');
            $table->integer('user_id')->unique();
            $table->integer('theme')->default(0);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('blog_info');
	}

}
