var gulp = require('gulp'),
	sass = require('gulp-ruby-sass');

gulp.task('sass', function() {
     return sass('resources/assets/sass/')
     		    .on('error', function (err) {
					console.error('Error!', err.message);
				})
				.pipe(gulp.dest('public/css/'));
});

gulp.task('watch', function() {
   gulp.watch('resources/assets/sass/*.sass', ['sass']);
 });

gulp.task('default',['sass', 'watch']);