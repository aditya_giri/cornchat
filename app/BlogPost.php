<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class BlogPost extends Model {
    use \Conner\Likeable\LikeableTrait;
	protected $fillable = ['blogname', 'category', 'title', 'user_id', 'post', 'slug'];
}
