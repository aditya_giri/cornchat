<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Twitter extends Model {

    protected $table = 'twitter';

    public function getUser()
    {
        return $this->belongsTo('User');
    }

}
