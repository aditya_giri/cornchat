<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class SearchController extends Controller {

	public function search()
	{
		$q = Request::get('q');
		$blogs = $q ? \App\BlogInfo::where('blogname', 'LIKE', "%$q%") : \App\BlogInfo::all();
        return view('blog', compact('blogs'));
	}

}
