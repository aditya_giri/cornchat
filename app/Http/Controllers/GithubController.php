<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Guzzle\Service\Client;
use Illuminate\Http\Request;

class GithubController extends Controller {

    public function user($id)
    {
        $client = new \Guzzle\Service\Client('https://api.github.com/');
        $user = $client->get("users/$id")->send();
        $array = $user->json();
        $repos = $client->get("users/$id/repos")->send();
        $repo = $repos->json();
        return view('github.user', compact('array', 'repo'));
    }

}
