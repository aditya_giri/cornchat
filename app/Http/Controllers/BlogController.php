<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class BlogController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
        $blogs = \App\BlogInfo::all();
        return view('allblogs', compact('blogs'));
	}

	public function showUserBlogs()
	{
		$blogs = \App\BlogInfo::where('user_id', \Auth::id())->get();
		
		return view('user.home', compact('blogs'));
	}

}
