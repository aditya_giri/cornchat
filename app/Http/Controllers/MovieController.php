<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Guzzle\Service\Client;
use Illuminate\Http\Request;

class MovieController extends Controller {

    public function index()
    {
        $client = new Client('http://api.filmon.com/api/vod/');
        if(isset($_REQUEST['name']) && isset($_REQUEST['genre']))
        {
            $response = $client->get('search?term=' . $_REQUEST['name'] . '&genre=' . $_REQUEST['genre'])->send();
            $info = $response->json();
            return view('movie.index', compact('info'));
        }
        elseif(isset($_REQUEST['name']))
        {
            $response = $client->get('search?term=' . $_REQUEST['name'])->send();
            $info = $response->json();
            return view('movie.index', compact('info'));
        }
        elseif(isset($_REQUEST['genre']))
        {
            $response = $client->get('search?term='.$_REQUEST['genre'] . '&max_results=200')->send();
            $info = $response->json();
            return view('movie.index', compact('info'));
        }
        $response = $client->get('genres')->send();
        $genres = $response->json();
        return view('movie.index', compact('genres'));
	}

    public function slug($slug)
    {
        $client = new Client('http://api.filmon.com/api/vod/');
        $response = $client->get('movie?id=' . $slug)->send();
        $info = $response->json();
        return view('movie.info', compact('info'));
    }

}
