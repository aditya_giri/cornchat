<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Google\Google;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class GoogleController extends Controller {

	public function index()
    {
        $client_id = "136529968341-7qrmt65v2qlb9mqr227udjoi8i18n49d.apps.googleusercontent.com";

        $redirect_uri = "http://localhost:8000/google/callback";
        $url = "http://accounts.google.com/o/oauth2/auth?scope=profile&redirect_uri=$redirect_uri&response_type=code&client_id=$client_id";
        return view('google.login', compact('url'));
    }

    public function callback()
    {
        if(isset($_REQUEST['code'])){
            Session::put('access_token', Google::get_oauth2_token($_REQUEST['code']));
        }


        return \Redirect::to('google');

    }


}
