<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Guzzle\Plugin\Oauth\OauthPlugin;
use Guzzle\Service\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class MailController extends Controller {

    public function index()
    {
        if(\App\User::find(\Auth::id())->gmail !== 0 && \App\Gmail::where('user_id', \Auth::id())->first() !== null) {
            $refresh_token = \App\Gmail::where('user_id', \Auth::id())->first()->refresh_token;
            $clientf = new Client();
            $responsef = $clientf->post('https://www.googleapis.com/oauth2/v3/token?client_id=136529968341-7qrmt65v2qlb9mqr227udjoi8i18n49d.apps.googleusercontent.com&client_secret=1O-TjsbDS-kYW6OyXvtui3xr&grant_type=refresh_token&refresh_token=' . $refresh_token)->send();
            $arrayf = $responsef->json();
            $mailcli = new Client('https://www.googleapis.com/gmail/v1/');
            $mailidsres = $mailcli->get('users/me/messages?access_token=' . $arrayf['access_token'] . '&maxResults=20')->send();
            $mailid = $mailidsres->json();
            return view('mail.home', compact('mailid', 'arrayf'));
        }
        return view('mail.home');
    }

    public function gcallback()
    {
        $client = new Client();
        $response = $client->post('https://www.googleapis.com/oauth2/v3/token?client_id=136529968341-7qrmt65v2qlb9mqr227udjoi8i18n49d.apps.googleusercontent.com&client_secret=1O-TjsbDS-kYW6OyXvtui3xr&code=' . $_REQUEST['code'] .'&grant_type=authorization_code&redirect_uri=http://localhost:8000/google/callback')->send();
        $array = $response->json();
        $user = new \App\Gmail;
        $user->user_id = \Auth::id();
        $user->refresh_token = $array['refresh_token'];
        $user->save();
        $userg = \App\User::find(\Auth::id());
        $userg->gmail = 1;
        $userg->update();
        return \Redirect::to('mail');
    }


}
