<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Str;

use Illuminate\Contracts\Filesystem\Filesystem;
use Illuminate\Http\Request;

class SiloController extends Controller {

	public function index()
	{
		return view('silo.form');
	}

	public function create(Request $request)
	{
		$blogname = strtolower($request->get('blogname'));
		$user = \App\User::find(\Auth::id());
		$user->blogname = $request->get('blogname');
		$user->update();
		$themeid = $request->get('theme');
		\App\BlogInfo::create([
			'blogname' => $blogname,
			'bloginfo' => $request->get('bloginfo'),
			'user_id'  => \Auth::id(),
			'image_name' => "none",
		]);
		return \Redirect::to('http://' . $request->get('blogname') . env('SITE_DOMAIN', '.livonair.com'));
	}

	public function admin($username)
	{
		$siloinfo = \App\BlogInfo::where("blogname", $username)->first();
		return view('silo.admin', compact('siloinfo'));
	}

	public function custom($username, Request $request)
	{
		$blog = \App\BlogInfo::where('blogname', $username)->first();
		$blog->analytics = $request->get('js');
		$blog->copyright = $request->get('copyright');

		if(!$request->file('image')->isValid())
			return Response::json(['error' => 'File is not valid']);
	 	
	 	if($request->file('image')){
			$blog->image_name = urlencode($request->file('image')->getClientOriginalName()) ? urlencode($request->file('image')->getClientOriginalName()) : "none";
			$image = $request->file('image');
			    $input = array('image' => $image);

			    $rules = array(
			        'image' => 'image'
			    );

			    $validator = \Validator::make($input, $rules);

			    if ($validator->fails())
			    {
			        return \Redirect::back()->with('status', 'error');
			    }

			$s3 = \Storage::disk('s3');
			$filePath = '/blog-profiles/' . urlencode($request->file('image')->getClientOriginalName());;
			$s3->put($filePath, file_get_contents($image), 'public');
		}

		$blog->update();
		return \Redirect::back()->with('status', 'info');
	}

	public function analytics($username)
	{
		$info = \App\Analytics::where('blogname', $username)->get(['pageviews', 'date']);
		return \Response::json($info);
	}

}
