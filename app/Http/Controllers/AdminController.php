<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\BlogInfo;
use App\User;
use Illuminate\Http\Request;

class AdminController extends Controller {

	public function index($username)
	{
		$blog = BlogInfo::where('blogname', $username)->first();
		$user = User::find($blog->user_id)->first();
		return view('admin.home', compact('blog', 'user'));
	}

	public function basic(Request $request, $username)
	{
		$blog = BlogInfo::where('blogname', $username)->first();
		BlogInfo::where('blogname', $username)->update(['bloginfo' => $request->get('tagline')]);
		$user = User::find($blog->user_id)->first();
		$user->update(['name' => $request->get('name')]);
		return "Updated Successfully";
	}

}
