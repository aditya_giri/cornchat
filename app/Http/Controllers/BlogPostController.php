<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\BlogInfo;
use App\BlogPost;
use Illuminate\Http\Request;

class BlogPostController extends Controller {

	public function category($username, $cat, $post = null)
	{
		if( ! $post == null )
		{
			$post = BlogPost::where('blogname', $username)->where('category', 'LIKE', "%$cat%")->where('slug', $post)->first();
			if( ! is_null($post) )
				$post = $post->toArray();
	        $user = \App\User::find($post['user_id']);
	        if(!is_null($user))
	        	$user = $user->toArray();
	        $comments = \App\Comments::where('post_id', $post['id'])->get();
	        $bloginfo = \App\BlogInfo::where('blogname', $username)->first()->toArray();
			return view('user.read', compact('post', 'user', 'comments', 'bloginfo'));
		}
		else
		{
			$category = $cat;
			$posts = BlogPost::where('blogname', $username)->where('category', 'LIKE', "%$cat%")->get();
			$user_id = BlogInfo::where('blogname', $username)->first();
			$user = \App\User::find($user_id->user_id);
			return view('blog.category', compact('posts', 'user', 'category'));
		}

	}

}
