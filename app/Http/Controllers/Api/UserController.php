<?php

use \App\User as User;

class UserController extends BaseController {
    public function all($email)
    {
        $user = User::where('email', $email)->first();
        return $this->response->item($user, new UserTransformer);
    }

}