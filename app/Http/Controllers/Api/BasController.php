<?php
use Illuminate\Routing\Controller;
use Dingo\Api\Routing\ControllerTrait;

class BaseController extends Controller
{
    use ControllerTrait;
}
