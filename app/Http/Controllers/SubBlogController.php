<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use \App\Favorites as Fav;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class SubBlogController extends Controller {

	public function index($username)
	{
		// Get blog info
		$bloginfo = \App\BlogInfo::where('blogname', $username)->first();
		if ($bloginfo === null) return \Redirect::to('/');
		//Get posts
		$posts = \App\BlogPost::where('blogname', $username)->get();
		//Get admin info
		$user = \App\User::find($bloginfo['user_id']);
		
		if(\Auth::id() !== $user->id){
			// Analytics
			$date = new \Carbon\Carbon(); 
			$analytics = \App\Analytics::where('blogname', $username)->where('date', date("Ymd") )->first();
			$analytics->pageviews = $analytics->pageviews + 1;
			$analytics->update();
		}
		
		return view('user.default', compact('posts', 'bloginfo', 'user'));
	}

	public function newPost($username)
	{
		if(\Auth::guest()) {return redirect('/auth/login');}
		$bloginfo = \App\BlogInfo::where('blogname', $username)->first();
		if(\Auth::id()!==$bloginfo['user_id']){return \Redirect::to('/'); }
		return view('user.newpost');
	}

	public function createPost(Request $request, $username)
	{
		$slug = $this->getSlug($request->title);
		\App\BlogPost::create([
			'blogname' => $username,
			'post' => $request->editor,
			'title' => $request->title,
			'slug'  => $slug,
			'category' => $request->tags,
			'user_id' => \Auth::id()
		]);
		return \Redirect::to('/');
	}

	public function readPost($username, $slug)
	{
		// Analytics
		$date = new \Carbon\Carbon(); 
		$analytics = \App\Analytics::where('blogname', $username)->where('date', date("Ymd"))->first();
		$analytics->pageviews = $analytics->pageviews + 1;
		$analytics->update();

		$post = \App\BlogPost::where('blogname', $username)->where('slug', $slug)->first()->toArray();
		$user = \App\User::find($post['user_id'])->toArray();
		$comments = \App\Comments::where('post_id', $post['id'])->get();
		$bloginfo = \App\BlogInfo::where('blogname', $username)->first()->toArray();
		return view('user.read', compact('post', 'user', 'comments', 'bloginfo'));
	}

	public function editPost($username, $slug)
	{
		$bloginfo = \App\BlogInfo::where('blogname', $username)->first();
		$user = \App\User::find($bloginfo->user_id);
		if(\Auth::id() === $user->id) {
			$post = \App\BlogPost::where('blogname', $username)->where('slug', $slug)->first();
			\Session::put('edit', 'true');
			return view('user.newpost', compact('post'));
		}
		return \Redirect::back();
	}


	private function getSlug($title)
	{
		return Str::slug($title);
	}

	public function comment(Request $request)
	{
		$comment = new \App\Comments;
		if (\Auth::guest()) {
			$comment->name = $request->get('name');
			$comment->email = $request->get('email');
		}else{
			$comment->name = \Auth::getUser()->name;
			$comment->email = \Auth::getUser()->email;
		}
		$comment->comment = $request->get('comment');
		$comment->post_id = $request->get('_postId');
		$comment->save();
		return \Redirect::back()->with('status', 'created');
	}

	public function deletePost($username, $slug)
	{
		$bloginfo = \App\BlogInfo::where('blogname', $username)->first();
		$user = \App\User::find($bloginfo->user_id);
		if(\Auth::id() === $user->id) {
			$post = \App\BlogPost::where('blogname', $username)->where('slug', $slug)->first();
			$post->delete();
			return \Redirect::to('/');
		}
		return \Redirect::back();
	}

}
