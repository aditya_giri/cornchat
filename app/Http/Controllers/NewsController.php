<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Guzzle\Service\Client;
use Illuminate\Http\Request;

class NewsController extends Controller {

    public function index()
    {
        return view('news.home');
    }

    public function nytimes()
    {
        $client = new Client('http://api.nytimes.com/svc/');
        $response = $client->get('topstories/v1/home.json?api-key=e3ec70f805b4675cf8f6fd6d49140f3c:17:72070023')->send();
        $array = $response->json();
        return view('news.nytimes', compact('array'));
    }

    public function guardian()
    {
        if(isset($_REQUEST['q'])){
            $client = new Client('http://content.guardianapis.com');
            $response = $client->get('/search?q=' . $_REQUEST['q'] . '&api-key=rcuyqdkwteb64rs7ycn5phw8')->send();
            $array = $response->json();
            return view('news.guardian', compact('array'));
        }
        return view('news.guardian');
    }


}
