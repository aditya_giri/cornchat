<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Guzzle\Http\Client;
use Illuminate\Http\Request;

class WeatherController extends Controller {

    public function index()
    {
        if(isset($_REQUEST['city'])){
            $client = new \Guzzle\Service\Client('http://api.openweathermap.org/data/2.5/');
            $response = $client->get('weather?q=' . $_REQUEST['city'] . '&APPID=7ca67a0452692a156bd4d9083fdb5091&units=metric')->send();
            return view('weather.main', compact('response'));
        }
        return view('weather.main');
    }


}
