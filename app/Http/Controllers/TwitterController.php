<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Thujohn\Twitter\Facades\Twitter;

class TwitterController extends Controller
{

    public function login()
    {
        $sign_in_twitter = true;
        $force_login = false;

        Twitter::reconfig(['token' => '', 'secret' => '']);
        $token = Twitter::getRequestToken(route('twitter.callback'));

        if (isset($token['oauth_token_secret'])) {
            $url = Twitter::getAuthorizeURL($token, $sign_in_twitter, $force_login);

            \Session::put('oauth_state', 'start');
            \Session::put('oauth_request_token', $token['oauth_token']);
            \Session::put('oauth_request_token_secret', $token['oauth_token_secret']);

            return \Redirect::to($url);
        }

        return \Redirect::route('twitter.error');
    }

    /** @noinspection PhpInconsistentReturnPointsInspection */
    public function callback()
    {

        if (\Session::has('oauth_request_token')) {
            $request_token = [
                'token' => \Session::get('oauth_request_token'),
                'secret' => \Session::get('oauth_request_token_secret'),
            ];

            Twitter::reconfig($request_token);

            $oauth_verifier = false;

            if (\Input::has('oauth_verifier')) {
                $oauth_verifier = \Input::get('oauth_verifier');
            }

            $token = Twitter::getAccessToken($oauth_verifier);

            if (!isset($token['oauth_token_secret'])) {
                return \Redirect::route('twitter.login')->with('flash_error', 'We could not log you in on Twitter.');
            }

            $credentials = Twitter::getCredentials();

            if (is_object($credentials) && !isset($credentials->error)) {
                $user = new \App\Twitter;
                $user->user_id = \Auth::user()->id;
                $user->token = $token['oauth_token'];
                $user->token_secret = $token['oauth_token_secret'];
                $user->save();
                $update = \App\User::find($user->user_id);
                $update->twitter = 1;
                $update->save();
                \Session::put('access_token', $token);
                return \Redirect::to('twitter');
            }

            return \Redirect::route('twitter.error')->with('flash_error', 'Crab! Something went wrong while signing you up!');
        }
    }

    public function post()
    {
        return view('twitter.post');
    }

    public function tweet(Request $request)
    {
        $tweet = $request->get('tweet');
        return Twitter::postTweet(['status' => $tweet, 'format' => 'json']);
    }

    public function user($id)
    {
        $info = Twitter::getUsers(['screen_name' => $id, 'format' => 'json']);
        $array = json_decode($info, true);
        if (strpos($array['profile_image_url'], 'png'))
            $image = substr_replace($array['profile_image_url'], '.png', -11);
        elseif (strpos($array['profile_image_url'], 'jpg'))
            $image = substr_replace($array['profile_image_url'], '.jpg', -11);
        elseif (strpos($array['profile_image_url'], 'jpeg'))
            $image = substr_replace($array['profile_image_url'], '.jpeg', -12);

        $tweetsjson = Twitter::getUserTimeline(['screen_name' => $id, 'count' => '200', 'format' => 'json']);
        $tweets = json_decode($tweetsjson);
        return view('twitter.user', compact('array', 'image', 'tweets'));
    }

    public function followers($id)
    {
        $followers_json = Twitter::getFollowers(['screen_name' => $id, 'count' => '2', 'format' => 'json']);
//        $followers = json_decode($followers_json, true);
        $info = Twitter::getUsers(['screen_name' => $id, 'format' => 'json']);
        $array = json_decode($info, true);
        if (strpos($array['profile_image_url'], 'png'))
            $image = substr_replace($array['profile_image_url'], '.png', -11);
        elseif (strpos($array['profile_image_url'], 'jpg'))
            $image = substr_replace($array['profile_image_url'], '.jpg', -11);
        elseif (strpos($array['profile_image_url'], 'jpeg'))
            $image = substr_replace($array['profile_image_url'], '.jpeg', -12);


        return view('twitter.followers', compact('followers_json', 'array', 'image'));
    }

    public function favourite()
    {
        $tweet_id = $_REQUEST['tweet'];
        Twitter::postFavorite(['id' => $tweet_id]);
        return \Redirect::to('twitter#'.$tweet_id);
    }

    public function destroyfav()
    {
        $tweet_id = $_REQUEST['tweet'];
        Twitter::destroyFavorite(['id' => $tweet_id]);
        return \Redirect::to('twitter#'.$tweet_id);
    }

    public function retweet()
    {
        $tweet_id = $_REQUEST['id'];
        Twitter::postRt($tweet_id);
        return \Redirect::to('twitter#'.$tweet_id);
    }


}