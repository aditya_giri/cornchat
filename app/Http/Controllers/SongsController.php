<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Guzzle\Service\Client;
use Illuminate\Http\Request;

class SongsController extends Controller {

    public function index()
    {
        if(isset($_REQUEST['q']))
        {
            $client = new Client('https://itunes.apple.com/');
            $response = $client->get("search?term=" . $_REQUEST['q'])->send();
            $array = $response->json();
            return view('songs.index', compact('array'));
        }
        return view('songs.index');
    }


}
