<?php

Route::group(['domain' => '{username}' . getenv('SITE_DOMAIN')], function(){
	Route::get('/api/json/analytics', 'SiloController@analytics');

	Route::controllers([
		'auth' => 'Auth\AuthController',
		'password' => 'Auth\PasswordController',
	]);
	
	Route::get('/', 'SubBlogController@index');
	Route::get('/post/{slug}', 'SubBlogController@readPost');
	Route::get('/post/{slug}/edit', 'SubBlogController@editPost');
	Route::get('/post/{slug}/delete', 'SubBlogController@deletePost');

	//Admin Routes
	Route::group(['middleware' => 'auth'], function () {
		Route::get('admin', 'AdminController@index');
		Route::post('admin', ['uses' => 'AdminController@basic', 'as' => 'admin']);
		Route::get('admin/new', 'SubBlogController@newPost');
		Route::post('admin/new', 'SubBlogController@createPost');
		Route::get("admin/edit", 'SiloController@admin');
		Route::post("admin/edit", 'SiloController@custom');
	});

	Route::post('post/comment', 'SubBlogController@comment');
	Route::get('/{cat}/{post?}', 'BlogPostController@category');

});

// API Needs to be done.

Route::get('/', function(){
	return view('homepage');
});





Route::get('songs', 'SongsController@index');
Route::get('movie', 'MovieController@index');
Route::get('movie/{slug}', 'MovieController@slug');
Route::get('weather', 'WeatherController@index');
Route::get('news/nytimes', 'NewsController@nytimes');
Route::get('news/guardian', 'NewsController@guardian');
Route::get('news', 'NewsController@index');
Route::get('/@{id}', 'TwitterController@user');




Route::get('listblogs', 'BlogController@index');




Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);



Route::group(['middleware' => 'auth'], function () {
	//Main Home and Twitter
	Route::get('twitter', 'HomeController@index');
	Route::get('blogs', 'BlogController@showUserBlogs');
	Route::get('twitter/favorite', 'TwitterController@favourite');
	Route::get('twitter/refavorite', 'TwitterController@destroyfav');
	Route::get('twitter/register', ['as' => 'twitter.login', 'uses' => 'TwitterController@login']);

	Route::get('twitter/callback', ['as' => 'twitter.callback', 'uses' => 'TwitterController@callback']);

	Route::get('twitter/error', ['as' => 'twitter.error', function(){
		// Something went wrong, add your own error handling here
	}]);


	Route::get('newsilo', 'SiloController@index');
	Route::post('newsilo', 'SiloController@create');

	Route::get('/github/{id}', 'GithubController@user');


	Route::get('/twitter/{id}/followers', 'TwitterController@followers');

});
