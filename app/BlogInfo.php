<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class BlogInfo extends Model {

	protected $table = 'blog_info';

    protected $fillable = ['user_id', 'bloginfo', 'blogname', 'theme', 'title', 'color', 'copyright', 'css', 'override-css', 'analytics', 'image_name'];

}
